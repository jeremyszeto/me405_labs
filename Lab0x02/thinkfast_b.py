''' @file thinkfast_b.py
    @brief A script that measures how quickly a user can react to an LED 
    flash by pressing a button.
    @details The user playing this "game" must attempt to react as quickly as 
    possible by pressing a button onboard the MCU when the LED flashes. Reaction
    times will be recorded until the user presses CTRL+C to end the game, then
    the average reaction time will be displayed.
    @package Lab0x02
    @brief This package contains thinkfast_a.py and thinkfast_b.py  
    @author Jeremy Szeto
    @date April 29, 2021
'''
import pyb, random, micropython
## @brief List of reaction times for calculating average.
reactions = []
## @brief Determines if button was pressed for new run
ic_capture = None
## @brief Holds OC compare time
prev_comp = 0
## @brief Initializes the blue user button
button = pyb.Pin(pyb.Pin.cpu.C13)
## @brief Initializes the LED
LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief Configures the IC interrupt pin
PB3 = pyb.Pin(pyb.Pin.cpu.B3)
## @brief Configures Timer 2 on the Nucleo
# @details The timer has a 0.1 ms resolution and a 6.55 second period.
timer = pyb.Timer(2, prescaler = 7999, period = 0xffff)
## @brief Configures Timer 2 Channel 2 for IC
IC = timer.channel(2, pyb.Timer.IC, pin = PB3, polarity = pyb.Timer.FALLING)

def press(pin):
    ''' @brief Interrupt callback function that runs when the button is pressed.
        @details This routine captures the time at which the interrupt was triggered 
            and stores it in a global variable
        @param pin The pin which triggers the interrupt service routine. This parameter is ignored.
    '''
    global ic_capture
    ic_capture = IC.capture()

# use the above function as a callback for IC
IC.callback(press)

def toggle(pin):
    ''' @brief Toggles the LED
        @details When the an output compare interrupt is triggered, this function toggles the LED.
                 If the LED is on, a 1 second is added to the compare value. 
                 If the LED is toggled off, a random value between 2-3 seconds 
                 is added to the compare value.
        @param pin The pin which triggers the interrupt service routine. This parameter is ignored.
    '''
    global prev_comp
    # prev_comp = pin.counter()
    if LED.value() == 1:
        print('Go!\n')
        # add 1 second to the OC compare value for the next comparison.
        prev_comp += 10000
    elif LED.value() == 0:
        print('A new run will start in 2 to 3 seconds. Ready...\n')
        prev_comp += random.randint(20000, 30000)
    # compensate for timer overflow
    prev_comp &= 0xffff
    # set the next OC compare value
    OC.compare(prev_comp)

## @brief Configures Timer 2 Channel 1 as an OC to toggle the LED
OC = timer.channel(1, mode = pyb.Timer.OC_TOGGLE, 
                   pin = LED, 
                   callback = toggle, 
                   polarity = pyb.Timer.HIGH)

if __name__ == "__main__":
    print('--------------------------------------------------------------------')
    print('Think Fast! When the green LED turns on, try to press the BLUE button\n'
          'as quickly as possible. The game will keep running until you press\n'
          'CTRL+C to stop. Are you ready?\n')
    print('Press ENTER to begin.')
    print('--------------------------------------------------------------------')
    input()
    try:
        # buffer to catch exceptions during interrupts
        micropython.alloc_emergency_exception_buf(100)
        while True:
            # Valid press
            if ic_capture is not None:
                ## @brief capture the reaction time
                reaction = ((ic_capture - prev_comp) & 0xffff) / 100.0
                print('Your reaction time was ' + str(reaction) + ' milliseconds, or '
                      + str(reaction / 10000.0) + ' seconds.')
                reactions.append(reaction)
                ic_capture = None
                # pressed = False
            # elif ic_capture is None and LED.value() == 1: 
            #     print('You took longer than 1 second to respond. Reaction time not recorded.')
    # CTRL + C detected: returns the user the average reaction time if available.        
    except KeyboardInterrupt:
        print('----------------------------------------------------------------')
        print('You pressed CTRL+C to end the game.')
        if len(reactions) > 0:
            ## @brief Computes the average reaction time
            avg = sum(reactions) / len(reactions)
            print('Average reaction time: ' + str(avg) + ' milliseconds, or ' 
                  + str(avg / 1000.0) + ' seconds.')
        else:
            print('You never pressed the button!')
        print('----------------------------------------------------------------')
        timer.deinit()
