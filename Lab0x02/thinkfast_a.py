''' @file thinkfast_a.py
    @brief A script that measures how quickly a user can react to an LED 
    flash by pressing a button.
    @details The user playing this "game" must attempt to react as quickly as 
    possible by pressing a button onboard the MCU when the LED flashes. Reaction
    times will be recorded until the user presses CTRL+C to end the game, then
    the average reaction time will be displayed.
    @package Lab0x02
    @brief This package contains thinkfast_a.py and thinkfast_b.py   
    @author Jeremy Szeto
    @date April 29, 2021
'''
import pyb, random, utime
## @brief Initialize button
button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
## @brief Initialize LED
LED = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT_PP)
## @brief Reaction time in microseconds
react = 0x7FFFFFFF
## @brief Determines if button was pressed for new run
pressed = False
## @brief List of reaction times for calculating average.
reactions = []
## @brief Start time in microseconds
start = 0

def isr(pin):
    ''' @brief Interrupt callback function that runs when the button is pressed.
        @details This routine captures the time at which the interrupt was triggered 
            and stores it in a global variable
        @param pin The pin which triggers the interrupt service routine. This parameter is ignored.
    '''
    global react, pressed, start
    pressed = True
    react = utime.ticks_diff(utime.ticks_us(), start)
            
## @brief The callback for the user button that activates on the falling edge.
extint = pyb.ExtInt(button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, isr)

if __name__ == "__main__":
    try:
        print('--------------------------------------------------------------------')
        print('Think Fast! When the green LED turns on, try to press the BLUE button\n'
              'as quickly as possible. The game will keep running until you press\n'
              'CTRL+C to stop. Are you ready?\n')
        print('Press ENTER to begin.')
        print('--------------------------------------------------------------------')
        input()
        while True:
            pressed = False
            print('A new run will start in 2 to 3 seconds. Ready...\n')
            pyb.udelay(random.randint(2000000, 3000000))
            print('Go!\n')
            start = utime.ticks_us()
            LED.on()
            pyb.udelay(1000000)
            LED.off()
            
            # Valid press
            if pressed:
                reactions.append(react)
                print('Your reaction time was ' + str(react) + ' microseconds, or '
                      + str(react / 1000000.0) + ' seconds.') 
            
            # Reaction time too slow
            else:
                print('You took longer than 1 second to respond. Reaction time not recorded.')
    
    # CTRL + C detected: returns the user the average reaction time if available.        
    except KeyboardInterrupt:
        print('----------------------------------------------------------------')
        print('You pressed CTRL+C to end the game.')
        if len(reactions) > 0:
            ## The average reaction time of the user over the course of the program (ms)
            avg = sum(reactions) / len(reactions)
            print('Average reaction time: ' + str(avg) + ' microseconds, or ' 
                  + str(avg / 1000000.0) + ' seconds.')
        else:
            print('You never pressed the button!')
        print('----------------------------------------------------------------')