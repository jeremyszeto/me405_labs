'''
@file mainpage.py
@mainpage

@section intro Introduction
This website serves as documentation for code developed by Jeremy Szeto in 
<b>ME405: Mechatronics</b> at California Polytechnic State University, San Luis 
Obispo.

@section course Course Description
<b> ME405: Mechatronics (4 Units) </b>

Microprocessor applications in machine control and product design. Applied 
electronics. Drive technology; transducers and electromechanical systems. 
Real-time programming. Mechatronic design methodology. 

@section portfolio Portfolio  
This is my ME405 Portfolio. See individual modules for more details.\n
\b Modules:
- @ref lab0x01
- @ref hw0x02
- @ref lab0x02
- @ref lab0x03
- @ref HW0x04
- @ref lab0x04
- @ref hw0x05
- @ref lab0xff

@section repo Source Code Repository
Here is a complete repository containing the source code of each module:
https://bitbucket.org/jeremyszeto/me405_labs/src/main/ \n

- - -
@author Jeremy Szeto
@copyright Copyright © 2021 Jeremy Szeto


@page lab0x01 Lab 0x01: Vendotron Finite State Machine

@section lab0x01_summary Summary
The objective of this assignment is to develop a program that embodies the 
function of a vending machine from a state transition diagram. State 
transition diagrams are high–level design tools that can be used to ensure 
that code will function according to stated design requirements. The 
Vendotron runs cooperatively; the code does not contain any blocking 
commands.

@section lab0x01_design Design Requirements
The Vendotron has several buttons, including one for each kind of drink
that is available. Additionally, there is a small two-line LCD text display 
and coin return button as you would expect on an average vending machine. 
Consider the figure below, which depicts Vendotron.

@image html vendotron.png "Figure 1: A diagram of the Vendotron (Source: Lab 0x01 Handout)"

The Vendotron accounts for the following behavior:
- On startup, Vendotron displays an initialization message on the LCD screen.
- At any time a coin may be inserted. When a coin is inserted, the balance 
displayed on the screen reflects so.
- At any time, a drink may be selected. If the current balance is sufficient, 
Vendotron vends the desired beverage through the flap at the 
bottom of the machine and then computes the correct resulting balance. If 
the current balance is insufficient to make the purchase then the machine 
displays an "Insufficient Funds" message and then displays the price for 
the selected item.
- At any time the Eject button may be pressed, in which case the machine 
returns the full balance through the coin return in the least number of 
denominations.
- The Vendotron prompts the user to select a second beverage if there is 
remaining balance.
- If the Vendotron is idle for a certain amount of time, it shows a 
"Try Cuke today!" scrolling message on the LCD display.\n

@section lab0x01_inputs User Inputs
<b>Beverage Selection:</b>\n
'C' - Cuke\n
'P' - Popsi\n
'S' - Spryte\n
'D' - Dr. Pupper\n\n

<b>Insert Payment</b>\n
'0' - penny\n
'1' - nickle\n
'2' - dime\n
'3' - quarter\n
'4' - dollar bill\n
'5' - five-dollar bill\n
'6' - ten-dollar bill\n
'7' - twenty-dollar bill\n\n
 
<b>Return Change</b>\n
'E' - Eject change\n\n

If any other key is pressed, an invalid input message will be displayed.\n

@section lab0x01_diagram Vendotron State Transition Diagram

@image html statediagram.png "Figure 2: Vendotron State Transition Diagram"

@section lab0x01_documentation Documentation
Documentation of this lab can be found here: Lab0x01

@section lab0x01_source Source Code 
The source code for this lab: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0x01/

- - -
@author Jeremy Szeto
@copyright Copyright © 2021 Jeremy Szeto


@page hw0x02 HW 0x02: Term Project System Modeling

@section hw0x02_summary Summary
This modeling assignment is the first step in preparation for the term 
project. Below are the steps showing development of a simplified model of a 
pivoting platform, which will aid in designing a controller to balance a ball 
atop said platform.\n

@image html platform.PNG "Figure 1: The platform (Source: HW 0x02 Handout)"
@image html hw2_step1.PNG "Figure 2: Simplified schematic"
@image html hw2_step2.PNG "Figure 3: Kinematic expression for the motion of the ball"
@image html hw2_step3.PNG "Figure 4: Relationship between the motor torque and moment applied to the platform"
@image html hw2_step4a.PNG "Figure 5: Equation of motion for the ball and platform system"
@image html hw2_step4b.PNG "Figure 6: Equation of motion for the ball as an isolated system"
@image html hw2_step5.PNG "Figure 7: Rearranging the EOMs from Figures 5 and 6"
@image html hw2_step6.PNG "Figure 8: Analytical model in matrix form"

- - -
@author Jeremy Szeto and Rebecca Rodriguez
@copyright Copyright © 2021 Jeremy Szeto


@page lab0x02 Lab 0x02: Think Fast!

@section lab0x02_summary Summary
The objective of this lab is to gain practice with writing embedded code 
which responds very quickly to an event using interrupts in MicroPython. 
The goal is to accurately measure a person's reaction time responding to a 
light and pressing a button.

@section lab0x02_design Design Requirements
The program waits a random time between 2 and 3 seconds, then turns on the 
green LED for 1 second. An interrupt is triggered when the user presses the 
blue button in response to the LED lighting up, allowing their reaction time 
to be measured. This repeats until the user inputs CTRL+C to end the game, 
then their average reaction time is displayed, or a message shown if they 
did not press the button at all.

@section lab0x02_overview Overview
To the user, both versions of this lab would appear very similar if not 
identical. However, to the programmer, there are some differences in terms of 
implementation. Part A of this lab uses the utime library and an external 
interrupt event when the button is pressed to calculate reaction time. The LED 
is toggled with a simple software delay. In contrast, Part B uses internal 
hardware and different channels on the onboard timer to handle interrupts. 
There is an input capture (IC) event which is triggered when the blue user 
button is pressed, and an output capture (OC) event which serves to toggle the 
LED every time there is a compare match. 

For applications that require high-resolution time measurements, using the 
second approach (Part B) may be more useful; however, for the purpose of this 
lab, the approach taken in Part A would suffice, since high precision is not 
necessary to measure human reaction time, which averages around 200 
milliseconds.

@section lab0x02_documentation Documentation
Documentation of this lab can be found here: Lab0x02

@section lab0x02_source Source Code 
The source code for this lab: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0x02/

- - -
@author Jeremy Szeto
@copyright Copyright © 2021 Jeremy Szeto

 
@page lab0x03 Lab 0x03: Pushing the Right Buttons

@section lab0x03_summary Summary
The objective of this lab is to examine the use of a digital device to measure 
analog inputs (button pushes). This is accomplished with a simple 
user interface facilitating communcation between a user and laptop, used to 
visualize how input voltage changes as a function of time.

@section lab0x03_design Design Requirements
There are two components to this lab: a front-end user interface and a 
back-end data collection program. The front-end script sends a character to 
the back-end to signal the back-end to begin data collection, and when the 
back-end has collected a good data sample, it sends these values back to the
front-end which generates a plot and .csv file from the data. The front-end 
and back-end components communicate with each other via UART serial 
communication.

@section lab0x03_results Results
A plot of the button step response is shown in Figure 1 below.

@image html ADC_plot.PNG "Figure 1: Button Step Response"

The theoretical calculation of the time constant can be found with T = RC.
From the Nucleo data sheet, R = 4.8 kOhms and C = 100 nF, giving a time 
constant T = 0.48 ms. From the step response, the time constant can be 
calculated by taking the inverse of the slope from the best fit equation. The 
slope was 2050.24 s, and the inverse 1/2050.24 = 0.488 ms, giving a percent 
difference of 1.67%. This is reasonable, since it is within the 5% tolerance 
of the ADC. This suggests that the experimental model is valid.

@image html ADC_bestfit.PNG "Figure 2: Button Step Response with Best Fit Line"

@section lab0x03_documentation Documentation
Documentation of this lab can be found here: Lab0x03

@section lab0x03_source Source Code 
The source code and .csv file for this lab: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0x03/

- - -
@author Jeremy Szeto
@copyright Copyright © 2021 Jeremy Szeto


@page HW0x04 HW 0x04: Linearization and Simulation of System Model
@htmlonly
<embed src="linearization_simulation.html" width="100%" height="7050px">
@endhtmlonly
 
@section hw0x04_source Source Code 
The source code for this assignment: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/HW0x04/

- - -
@author Jeremy Szeto and Rebecca Rodriguez
@copyright Copyright © 2021 Jeremy Szeto


@page lab0x04 Lab 0x04: Hot or Not?

@section lab0x04_summary Summary
The objective of this assignment is to create and test a driver for an I2C-
connected MCP9808 sensor and use the sensor to log temperature data.

@section sec_lab0x04_I2C I2C Communications
THe MCP9808 temperature sensor uses an I2C interface to communicate with a 
microcontroller. Many other sensors such as accelerometers, IMUs, humidity 
sensors, \a etc. use the same method as this sensor. An MCP9808 on a 
breakout board can be used with only its power, ground, and two I2C 
communication pins connected as shown in Figure 1 below.\n

@image html i2c_pinout.PNG "Figure 1: An I2C device connected to the Nucleo MCU (Source: Lab 0x04 Handout)"

The I2C interface uses two wires to carry data to and from sensors or other
devices that work with a microcontroller. It is used over short distances,
typically less than a meter or so.\n\n
The I2C communication wires are:\n
- SDA or Serial Data, which carries data one bit at a time in any needed 
direction
- SCL or Serial Clock, a clock which synchronizes data transmission and 
reception\n
.
The I2C interface is a \a bus, meaning that the SDA and SCL lines may be 
shared between many devices. To distinguish between different devices, each 
device is assigned a 7-bit I2C bus address. The address may be set at the 
factory; many devices have address control pins which can be used to change 
the address, allowing several devices of one type to share an I2C bus. A 
few devices can have their addresses programmed by the user. An I2C 
communication begins with a \a start \a condition (I2C talk for "Hey!") and 
the microcontroller sending the 7-bit address of the device with which it 
will exchange data. Along with the address, an eighth bit indicates whether 
the microcontroller will write data to the device or read data from it. Then 
one or more bytes are exchanged.\n\n

Most I2C devices must communicate many numbers with the microcontroller -- 
for example, two bytes of temperature data, another two bytes of 
configuration, and so on. These numbers are kept in \a registers. After the 
microcontroller has transmitted a device address and the read/write bit, it 
transmits a \a register \a address, the location inside the device at which 
the number of interest is found. One or several bytes of data can be 
exchanged at a time. The image in Figure 2 below shows part of an I2C 
communication as viewed with a $13 logic analyzer.\n

@image html i2c_signal.PNG "Figure 2: I2C communication waveform (Source: Lab 0x04 Handout)"

@section lab0x04_design Design Requirements
1. A script for the microcontroller which measures the internal temperature
   of the microcontroller using the pyb.ADCAll class and saves it to a file. 
   A reference for the ADCAll class is 
   https://docs.micropython.org/en/latest/library/pyb.ADC.html.
2. A Python module called mcp9808.py containig a class which allows the user 
   to communicate with an MCP9808 temperature sensor using its I2C 
   interface. The class should contain the following methods:
   - An initializer which is given a reference to an already created I2C 
     object and the address of the MCP9808 on the I2C bus
   - A method \b check() which verifies that the sensor is attached at the 
     given bus address by checking that the value in the manufacturer ID 
     register is correct
   - A method \b celsius() which returns the measured temperature in degrees 
     Celsius
   - A method \b fahrenheit() which returns the measured temperature in 
     degrees Fahrenheit
3. A main.py which uses the mcp9808 driver module and code from step 1 to 
   take temperature readings approximately every sixty seconds from the 
   STM32 and from the MCP9808, and saves those readings in a CSV file on the 
   microcontroller. The CSV file’s columns should be time, STM32 
   temperature, and ambient (MCP9808) temperature. Data should be taken 
   until the user presses Ctrl+C, at which time the program should cleanly 
   exit with no errors.
4. Take data for a period of at least eight hours, then retrieve the data file
   from the microcontroller and plot it.

@section lab0x04_results Results

@image html temperature.PNG "Figure 3: Temperature data measured by a Nucleo and MCP9808 temperature sensor between 2:15am and 10:15am on 5/11/21 in San Luis Obispo, CA."

As seen in Figure 3, the Nucleo core temperature is consistently around 2.5°C 
higher than the MCP9808 temperature. This is most likely because the Nucleo 
processor dissipates heat as it operates. The plot also shows that the MCP9808 
sensor has a significantly higher resolution than the Nucleo ADC, which is 
only 12 bits and is therefore limited to a maximum resolution of 0.0625 °C.

@section lab0x04_documentation Documentation
Documentation of this lab can be found here: Lab0x04

@section lab0x04_source Source Code 
The source code and .csv file for this lab: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0x04/

- - -
@author Jeremy Szeto
@copyright Copyright © 2021 Jeremy Szeto


@page hw0x05 HW 0x05: Pole Placement (Full State Feedback)

@htmlonly
<embed src="pole_placement.html" width="100%" height="4800px">
@endhtmlonly
 
@section hw0x05_source Source Code 
The source code for this assignment: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/HW0x05/

- - -
@author Jeremy Szeto and Rebecca Rodriguez
@copyright Copyright © 2021 Jeremy Szeto


@page lab0xff Lab 0xFF: Term Project

@section lab0xff_summary Summary
The culminating term project for the course involves developing a program to 
balance a ball atop a 2-DoF platform, building upon the system model and 
controller developed in the homework assignments. It is broken into separate 
pieces, including:
- @ref rtp
- @ref pwm
- @ref enc
- @ref imu
- @ref ball

@section rtp Reading from a Resistive Touch Panel
This module sets up the resistive touch screen that detects the position of 
the ball on the platform. The resistive touch panel driver makes it possible 
to track the planar position of the ball on the platform. The screen is 176.00 
mm x 99.36 mm; however, the active measurement area is not the same as the 
screen area. Therefore, some calibration is needed to obtain accurate touch 
panel position coordinates. This is done in my code by measuring the distance 
in millimeters between two points on the touch panel and also recording their 
respective ADC counts. Then, the absolute position is a ratio between measured 
distance and ADC distance, offset by the distance to the count at the center 
of the panel to map accurately to my defined origin in the physical center of 
the panel.

Figure 1, taken from the lab manual, is a schematic representation of a 4-wire 
resistive touch panel.

@image html res_touchPanel.PNG  "Figure 1. 4-Wire Resistive Touch Panel Schematic Representation"

The touch panel driver scans the x and y position of the ball, as well as 
detects whether or not the ball is in contact with the screen. There is a 
delay of 4 microseconds between each x-, y-, and z-scan, since it takes 
approximately 3.6 microseconds for the ADC count reading to settle. The screen 
driver was able to read from a single channel in less than 500 microseconds as 
well as read all three channels in less than 1500 microseconds. This is 
critical for the driver to work cooperatively with other code. The results 
from PuTTY are shown below:
\code{.py}
execfile('touchPanelDriver.py')
x-Scan Time [microseconds]: 500, x-value [cm]: 5.560221
y-Scan Time [microseconds]: 401, y-value: [cm] 8.715
z-Scan Time [microseconds]: 376, Point of contact? True
xyz-Scan Time [microseconds]: 1143, x: [cm] 5.049171, y: [cm] 8.155, Point of contact? True

\endcode
All three channels were able to be polled in under 1500 microseconds as shown 
above with 'xyz-Scan'; however, the time was further reduced to 492 
microseconds by optimizing the number of pin configurations. 

The output in PuTTY shown below shows a further decrease in scanning time for 
all three channels with the 'quick-Scan' method:
\code{.py}
quick-Scan Time [microseconds]: 492, x: [cm] 4.865193, y: [cm] 8.225, Point of contact? True
\endcode

The calibration of the touch screen was verified by marking the touch screen 
with tape and lightly placing the tape on the screen as pictured below.

@image html cal_screen.jpg  "Figure 2. Calibration Verification of the Resistive Touch Screen"

See https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/touchPanelDriver.py 
for the file documentation.

@section pwm Driving DC Motors with PWM and H-Bridges
The balancing platform utilizes two DC motors (one motor controls each planar 
axis). The driver is written for the DRV8847 dual H-bridge motor. The encoders 
attached to the motors are used to determine the angular velocity of the 
platform about each rotational axis. The motor driver then takes in a single 
duty cycle value that relates to the torque applied to the platform. 
Furthermore, a safety fault detection was implemented to stop the motor 
when a fault is detected and prevent damage to the hardware. Testing showed 
that the duty cycle to overcome static friction on each motor was at least 
40%. The motor arm was attached to the platform as shown in Figure 3.

@image html motorArmMount.jpg  "Figure 3. DC Motor and Arm Placement"

See https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/motorDriver.py 
for the file documentation.

@section enc Reading from Quadrature Encoders
Each motor on the balancing platform has an associated Quadrature Encoder. The 
encoder is used to accurately measure the amount of rotation of each motor in 
either direction, and the driver has been written to compensate for over or 
underflow when the encoder value reaches the maximum of 65536. This scenario 
is avoided by resetting the encoder measurement every iteration of the FSM 
controller, such that the values do not get as large as 65536. The results 
from PuTTY are shown below:
\code{.py}
execfile('encoderDriver.py')
Testing for overflow...
Encoder Position: 65548
Moving motor...
Encoder Position: 4

Testing for underflow...
Encoder Position: 20
Moving motor...
Encoder Position: -16

\endcode

See https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/encoderDriver.py 
for the file documentation.

@section imu Reading from an Inertial Measurement Unit
@subsection sec_t3 Reading from Inertial Measurement Unit
The IMU (BNO055) was soldered to one corner of the platfrom and powered with 
5VDC. The sensor communicates using I2C protocol so there are two pins 
required for communication. The pins are the SCL (clock) and SDA (data) pins 
and wiring is as shown in Figure 4.

@image html imu.PNG  "Figure 4. IMU Setup and Placement on Platform"

The IMU is able to determine the orientation of the platform as well as the 
angular velocity of the platform. The sensor outputs are necessary for 
feedback control for the balancing platform. Fortunately, a driver was found 
for the BNO055 IMU written in MicroPython. The driver includes the following 
methods: a method to change the operating mode of the IMU, a method to 
retrieve the calibration status, and a method to read the Euler angles. The 
Euler angles are output in degrees in the form of (heading, roll, pitch) as 
shown in Figure 5.

@image html EulerAng.PNG  "Figure 5. Euler Angles Reference"

Calibration of the IMU is as simple as stabilizing the platform in a set 
position for a few seconds to calibrate the gyroscope. Calibration was 
complete once the calibration status method returned a value of 3. The angular 
velocity was obtained from recording the change in Euler angles with respect 
to time. This driver was incorporated in the final design to obtain readings 
of the platform's roll and pitch, which was necessary for the closed loop 
control of the balancing platform.

Please see https://github.com/micropython-IMU/micropython-bno055#readme for 
the reference of the BNO055 driver.

@section ball Balancing the Ball
In the last step of the project all hardware, drivers, and FSM controller were 
combined to form the closed-loop control system for the balancing ball on the 
platform. The position and velocity of the ball is obtained from the touch 
panel, while the angle and anglular velocity of the platform is obtained from 
the IMU. One motor applies a torque about the x-axis, while the other motor 
applies a torque about the y-axis. The torque required to balance the platform 
is related to the duty cycle by:

@image html duty_eq.gif

Where the matrix for gains is expressed as:

@image html gains.gif

The matrix for the state variables is:

@image html state_var.gif

And the parameters obtained from the DC motor datasheet are:

@image html param.gif

In summary, a torque about the x-axis rotates the platform an amount theta_y 
which in turn moves the ball along the x-axis. The gains were previously 
calculated to be K = [-203.10220296  -28.97855503  -42.42285661  -2.25228044]. 
However, due to friction from the ball joints connecting the motor arms to the 
platform, these values needed to be modified slightly. More information on the 
method used to analytically determine controller gains can be found in @ref hw0x02 and 
@ref HW0x04, and tuning the controller gains can be found in @ref hw0x05.

The state diagram for the project is shown below:
@image html TermProjectFSM.PNG

@section results Results

For the most part, the ball balancing platform worked as intended. However, 
one major issue I ran into was the fact that the motor arms did not couple 
tightly with the motor shaft. To solve this issue, I used a paper towel shim 
to slightly increase the diameter of the motor shaft. However, one other 
problem I had was the lack of adequate tension in the motor belt. The belt 
tended to slip slightly when there was high torque output form the motor, 
causing the platform to be in a different position than where the encoder 
measurements indicated that it should be. This sometimes caused the program 
to malfunction and overshoot instead of stabilizing the ball in the center of 
the platform, causing the "equilbrium" point to move to a different position on 
the board or make the system unstable entirely and cause the ball to roll off 
of the platform. Below are some graphs of the system response. The images have 
been cropped to show only the relevant sections.

@image html xpos.PNG "X position [mm] vs. time [s]"
@image html ypos.PNG "Y position [mm] vs. time [s]"
@image html xvel.PNG "X velocity [mm/s] vs. time [s]"
@image html yvel.PNG "Y velocity [mm/s] vs. time [s]"


@section demo Video Demonstration
A video demonstration of the working system can be found here: 
https://photos.app.goo.gl/ncvvAbxbmMFGdNgXA

@section lab0xff_documentation Documentation
Documentation of this lab can be found here: Lab0xFF

@section lab0xff_source Source Code 
The source code for the term project: 
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/Rebecca's%20Drivers/ and
https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/

- - -
@author Jeremy Szeto and Rebecca Rodriguez
@copyright Copyright © 2021 Jeremy Szeto

'''
