''' @file ui.py
    @brief A front-end user interface to visualize voltage data from a button press.
    @details A front-end user interface that runs on a PC and receives user input.
             When the user presses \‘G,\’ the Nucleo™ awaits a button press.
             When data collection is complete, the script generates a plot of voltage vs. time
             (using matplotlib) and a .CSV file of data (timestamps and ADC counts).
    @package Lab0x03
    @brief This package contains main0x03.py and ui.py.
    @author Jeremy Szeto
    @date May 6, 2021
'''
import serial
import keyboard
from matplotlib import pyplot as plot

## @brief Initialize serial port to communicate with the Nucleo.
ser = serial.Serial(port = 'COM3', baudrate = 115200, timeout = 1)  
## @brief Array that holds the timestamps of each ADC value.
times = []
## @brief Array that holds the ADC values.
values = []
## @brief Holds the last key that a user pressed
last_key = ''

def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
        @param key String representation of the key pressed by the user.
        @return last_key String representation of the last key pressed by the 
                user, stored as a global variable.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to this particular key only
keyboard.on_release_key("g", callback=kb_cb)

def run():
    ''' @brief FSM that controls the user interface.
    '''
    global times, values
    ## @brief State 0: Initialization
    S0_INIT = 0    
    ## @brief State 1: Wait for a response from the Nucleo
    S1_WAIT = 1
    ## @brief State 2: Parse data sent by the Nucleo
    S2_EXPORT = 2
    ## @brief The state to run on the next iteration
    state = S0_INIT

    while True:
        # State 0 Code
        if state == S0_INIT:
            global last_key
            print('To start, press \'G\'.')
            while(last_key != 'g' and last_key != 'G'):     # block until g is pressed
                pass
            ser.write('G'.encode('ascii'))                  # send g to Nucleo
            print('Press the blue button to record data.')
            state = S1_WAIT
            
        # State 1 Code
        elif(state == S1_WAIT):
            if str(ser.readline().decode('ascii')).strip() == 'START':
                state = S2_EXPORT
                
        # State 2 Code      
        elif(state == S2_EXPORT):
            echo = ser.readline().decode('ascii').strip()
            while echo != 'END':                            # add points until 'END' message is sent
                point = echo.strip().split(',')
                times.append(float(point[0]))
                values.append(float(point[1]))
                echo = ser.readline().decode('ascii').strip()
            export()
            ser.close()
            break

def export():
    ''' @brief Plots the button press voltage vs. time and exports the individual points to a .csv file
        @details The plot is exported as "ADC_plot.png", and the data is exported as "ADC_data.csv".
    '''
    plot.plot(times, values, '.')                           # Plot ADC value vs time datapoints
    plot.xlabel('Time, t [ms]')                             # Label x-axis
    plot.ylabel('ADC Value of Button Press Voltage [V]')    # Label y-axis
    plot.title('Button Press Voltage vs. Time')             # Plot title
    plot.savefig('ADC_plot.png')                            # Save the plot
    print('The plot has been exported as "ADC_plot.png"')

    # Generate .csv file
    with open('ADC_data.csv','w') as fp:
        # Go through each row of data
        for i in range(len(times)):
            fp.write('{:f},{:f}\n'.format(times[i], values[i])) # Write a row of a data
                    
    print('The data has been exported as "ADC_data.csv"')

if __name__ == '__main__':    
    run() # Continuously run the FSM