''' @file main0x04.py
    @brief This file contains a script used to measure and plot temperature data from an MCP9808 temperature sensor.
    @details This script interfaces with an MCP9808 temperature sensor via I2C 
             serial communication.
    @package Lab0x04
    @brief This package contains mcp9808.py and main0x04.py.
    @author Jeremy Szeto
    @date May 13, 2021
'''
import pyb
from pyb import I2C
import utime   
from mcp9808 import MCP9808

## @brief initialize ADC to 12-bit resolution on channels 16-18 
adc = pyb.ADCAll(12, 0x70000)
adc.read_core_vref() # use the internal Nucleo reference voltage
## @brief I2C object that sets the Nucleo as the master
i2c = I2C(1, I2C.MASTER, baudrate = 115200)
## @brief MCP9808 object
sensor = MCP9808(i2c, 0)

try: 
    if sensor.check(): 
        print('Sensor found. Recording temperature data...')
        print('To stop recording, press CTRL+C.\n')
        ## @brief time that recording started in milliseconds
        start = utime.ticks_ms()
        ## @brief The maximum number of milliseconds before script times out (8 hours)
        end = start + 1000 * 60 * 60 * 8 

        with open('temperature.csv', 'w') as fp:
            while utime.ticks_ms() < end:
                ## @brief record time in seconds
                time = utime.ticks_diff(utime.ticks_ms(), start) / 1000 # record time in seconds
                ## @brief read STM32 temperature
                stm32_temp = adc.read_core_temp() # read STM32 temp
                ## @brief read MCP9808 temperature
                mcp9808_temp = sensor.celsius()   # read MCP9808 temp
                
                print('Time: ' + str(time) + 's')
                print('STM32 temp: ' + str(stm32_temp) + ' C')
                print('MCP9808 temp: ' + str(mcp9808_temp) + ' C\n')
                        
                fp.write('{:},{:},{:}\r\n'.format(time, stm32_temp, mcp9808_temp))
                utime.sleep(60) # wait 60s before taking next measurement
            print('Maximum recording time reached.\n')
    else:
        print('Sensor not found.\n')
except KeyboardInterrupt:
    print('You pressed CTRL+C to stop recording data.\n')
