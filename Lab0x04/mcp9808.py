''' @file mcp9808.py
    @brief A driver class that allows the user to commmunicate with an MCP9808 temperature sensor via I2C
    @package Lab0x04
    @brief This package contains mcp9808.py and main0x04.py.
    @author Jeremy Szeto
    @date May 13, 2021
'''
import pyb
from pyb import I2C

class MCP9808:
    ''' @brief A driver class that allows the user to commmunicate with an MCP9808 temperature sensor via I2C
        @details This class contains code to create a MCP9808 object which can 
                 interface with an I2C object via the given bus address. Objects 
                 of this class can verify that the sensor is attached at the 
                 given bus address by checking that the value in the manufacturer
                 ID register is correct and return the measured temperature in 
                 degrees Celsius or Fahrenheit.
    '''
    # Datasheet: https://cdn-shop.adafruit.com/datasheets/MCP9808.pdf
    # Register Pointer Addresses specified on Datasheet pg. 16
    ## @brief Temperature register
    T_A = 5
    ## @brief Manufacturer ID register
    MANUFACTURER_ID = 6
    
    def __init__(self, i2c, address):
        ''' @brief Instantiates a new MCP9808 object
            @param i2c The I2C object used for serial communication
            @param address An integer representing the bus address of the sensor
        '''
        ## @brief I2C object used for serial communication
        self.i2c = i2c
        ## @brief the bus address of the sensor
        self.address = 0b11000 + address
        ## @brief holds data sent via I2C
        self.buffy = bytearray(2)       # buffer holds 2 bytes of data from I2C
        ## @brief the manufacturer ID of the temperature sensor
        self.id = 0x0054                # manufacturer id (pg. 27 of datasheet)
        
    def check(self):
        ''' @brief Verifies that the sensor is attached at the given bus address
            @details Verifies that the sensor is attached at the given bus address by
                     checking that the value in the manufacturer ID register is correct.
            @return Boolean representing whether the value in the manufacturer ID register is correct.
        '''
        self.i2c.mem_read(self.buffy, self.address, self.MANUFACTURER_ID)
        return self.id == self.buffy[1]
                
    def celsius(self):
        ''' @brief Measures the temperature in degrees Celsius
            @return temp A float representing the measured temperature in degrees Celsius
        '''  
        # See pg. 25 of datasheet
        self.i2c.mem_read(self.buffy, self.address, self.T_A)
        upper_byte = self.buffy[0] # Read 8 bits and send ACK bit
        lower_byte = self.buffy[1] # Read 8 bits and send NAK bit
        upper_byte &= 0x1F         # Clear flag bits
        
        if ((upper_byte & 0x10) == 0x10): # T_A < 0°C
            upper_byte &= 0x0F # Clear SIGN
            temp = 256 - (upper_byte * 16 + lower_byte / 16)
        else: # T_A >= 0°C
            temp = (upper_byte * 16 + lower_byte / 16);
        return temp
    
    
    def fahrenheit(self):
        ''' @brief Measures the temperature in degrees Fahrenheit
            @return A float representing the measured temperature in degrees Fahrenheit
        '''
        return (self.celsius() * 9.0 / 5.0) + 32.0

if __name__ == '__main__':
    # test code:
    ## @brief I2C object that sets the Nucleo as the master
    i2c = I2C(1, I2C.MASTER, baudrate = 115200)
    ## @brief instantiate MCP9808 object
    sensor = MCP9808(i2c, 0)
    try:
        if sensor.check():
            while True:
                print('Temperature in Celsius   : ' + str(sensor.celsius()))
                print('Temperature in Farenheit : ' + str(sensor.farenheit()))
                print('')
                pyb.delay(1000)
        else:
            print('Sensor not found.\n')
    except KeyboardInterrupt:
        print('You pressed CTRL+C to stop recording data.\n')
