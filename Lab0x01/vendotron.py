''' @file vendotron.py
    @brief This file contains a finite state machine that simulates a Vendotron vending machine.
    @package Lab0x01
    @brief This package contains vendotron.py
    @author Jeremy Szeto
    @date April 22, 2021
''' 
import keyboard
from time import sleep

## @brief The user's key input.
global last_key
last_key = None
    
def keystroke(key):
    ''' @brief Callback which runs when the user presses a key.
        @param key String representing the last key pressed by the user.
        @return last_key Stored as a global variable
    '''
    global last_key
    last_key = key.name
    
def VendotronTask():
    ''' @brief Runs one iteration of the FSM task.
        @details The vendotron FSM can be in one of six states: initiatization,
                 wait for user input, insert money, select a drink, calculate 
                 new balance, eject, or idle. Once called, this function will 
                 continuously run a loop which checks the state of the FSM and 
                 responds appropriately. To exit the loop, the user can press 
                 ctrl+c.
        @return state The current state of the vendotron.
    '''    
    ## @brief A list of the possible currency denominations
    currencyTypes = ['penny(s)', 'nickle(s)', 'dime(s)', 'quarter(s)', 'dollar(s)', 'five(s)', 'ten(s)', 'twenty(s)']
    ## @brief A list of the drink keys
    drinkKeys = ['c', 'C', 'p', 'P', 's', 'S', 'd', 'D']
    ## @brief A list of the drink types
    drinkNames = ['Cuke', 'Popsi', 'Spryte', 'Dr. Pupper']
    ## @brief A list of the drink prices
    drinkPrices = [100, 120, 85, 110]
    ejectKeys = ['E', 'e']
    ## @brief A list of the payment keys
    coinKeys = ['0', '1', '2', '3', '4', '5', '6', '7']
    ## @brief A list of the payment types
    coinNames = ['Penny', 'Nickle', 'Dime', 'Quarter', 'Dollar', 'Five', 'Ten', 'Twenty']
    ## @brief A list representing the number of each payment denomination the user has inserted into the Vendotron
    payment = [0, 0, 0, 0, 0, 0, 0, 0]
    
    ## @brief State 0: Initialization
    S0_INIT = 0
    ## @brief State 1: Wait for user input
    S1_WAIT = 1
    ## @brief State 2: Payment has been inserted
    S2_INSERT_COIN = 2
    ## @brief State 3: Drink has been selected
    S3_SELECT_DRINK = 3
    ## @brief State 4: User has enough balance to buy drink
    S4_SUFFICIENT_BALANCE = 4
    ## @brief State 5: Eject button has been pressed
    S5_EJECT = 5
    ## @brief State 6: No input has been entered for some time
    S6_IDLE_MSG = 6
    
    ## @brief the first state of the Vendotron
    state = S0_INIT
    
    keyboard.on_press(keystroke)
    while True:
        # Run State 0 Code    
        if(state == S0_INIT):
            payment = [0, 0, 0, 0, 0, 0, 0, 0]
            idle_time = 0
            print('Thank you for choosing Vendotron!')
            print('Please insert coins, or select a beverage.\n')
            state = S1_WAIT

        # Run State 1 Code
        elif(state == S1_WAIT):
            if idle_time > 1500:
                state = S6_IDLE_MSG
                
            global last_key
            if last_key is None:
                idle_time += 1
                
            elif last_key in ejectKeys:
                state = S5_EJECT
                
            elif last_key in coinKeys:
                state = S2_INSERT_COIN
                
            elif last_key in drinkKeys:
                state = S3_SELECT_DRINK
                
            elif (last_key not in ejectKeys 
                or last_key not in coinKeys 
                or last_key not in drinkKeys 
                or last_key is not None):
                last_key = None
                print('Sorry, that is an invalid input.')
                print('C = Cuke, P = Popsi, S = Spryte, D = Dr. Pupper, E = Eject\n')
                
        # Run State 2 Code
        elif(state == S2_INSERT_COIN):
            print('You inserted a ' + str(coinNames[int(last_key)]) + '.')
            payment[coinKeys.index(last_key)] += 1
            print('Current balance: $' + str(format(getBalance(payment)/100.0, '.2f')) + '\n')
            last_key = None
            idle_time = 0
            state = S1_WAIT
            
        # Run State 3 Code
        elif(state == S3_SELECT_DRINK):
            drink = str(drinkNames[drinkKeys.index(last_key)//2])
            cost = str(format((drinkPrices[drinkKeys.index(last_key)//2])/100.0, '.2f'))
            # print("You selected " + drink + ", which costs $" + cost)
            
            if(getBalance(payment) >= drinkPrices[drinkKeys.index(last_key)//2]):
                state = S4_SUFFICIENT_BALANCE

            else:
                print('Insufficient Funds. ' + drink + ' costs ' + cost + '.')
                print('Current balance: $' + str(format(getBalance(payment)/100.0, '.2f')) + '. Please insert more money.\n')
                state = S1_WAIT
                last_key = None
                idle_time = 0
        
        # Run State 4 Code
        elif(state == S4_SUFFICIENT_BALANCE): 
            if(getBalance(payment) > drinkPrices[drinkKeys.index(last_key)//2]):
                payment = getChange(drinkPrices[drinkKeys.index(last_key)//2], payment)
                print(drink + ', which costs $' + cost + ', has been dispensed.')
                print('Current balance: $' + str(format(getBalance(payment)/100.0, '.2f')) + '. Please make another selection.\n')
                state = S1_WAIT

            elif(getBalance(payment) == drinkPrices[drinkKeys.index(last_key)//2]):
                payment = getChange(drinkPrices[drinkKeys.index(last_key)//2], payment)
                print(drink + ', which costs $' + cost + ', has been dispensed.')
                print('Current balance: $' + str(format(getBalance(payment)/100.0, '.2f')) + '\n')
                print('------------------------------------------\n')
                state = S0_INIT
            
            last_key = None
            idle_time = 0

        # Run State 5 Code
        elif(state == S5_EJECT): 
            change = getChange(0, payment)
            print('Returning your total balance of: $' + str(format(getBalance(payment)/100.0, '.2f')) + '\n')
            for i in range(len(change)):
                if change[i] > 0:
                    print('Ejecting ' + str(change[i]) + ' ' + currencyTypes[i])

            print('\n------------------------------------------\n')
            last_key = None
            idle_time = 0
            state = S0_INIT

        # Run State 6 Code
        elif(state == S6_IDLE_MSG):
            print('No input detected.')
            print('Try Cuke today!\n')
            idle_time = 0
            state = S1_WAIT
        
        yield(state)

def getChange(price, payment):
    ''' @brief Computes correct change for a given purchase.
        @details This function takes the cost and payment of an item and returns change in the fewest denominations possible.
        @param price An integer representing the price of an item in cents.
        @param payment A list representing the number of pennies, nickles, dimes, quarters, dollars, fives, tens, and twenties in ascending order.
                       Ex. payment = [3, 0, 2, 1, 0, 0, 1] refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
        @return A list in the same format as the payment list input parameter, or returns the payment list if there are insufficient funds.
    '''
    ## @brief A tuple representing the value of each currency denomination in cents
    amounts = (2000, 1000, 500, 100, 25, 10, 5, 1)
    ## @brief A list holding the change after completing a purchase with the given payment
    change = [0, 0, 0, 0, 0, 0, 0, 0]
                                              
    remainder = getBalance(payment) - price
    if remainder < 0:
        return payment

    for i in range(len(amounts)):
        if remainder >= amounts[i]:
            change[i] = remainder // amounts[i]
            remainder %= amounts[i]
                        
    ## Return the change as a list in the same format as the input payment list
    return change[::-1]
    
def getBalance(payment):
    ''' @brief Calculates the user's balance.
        @details This function the converts a list of a quantity of denomincations of currency (ex. pennies, nickles...)
                 into an integer representing the total balance in cents.
        @param payment A list representing the number of pennies, nickles, dimes, quarters, dollars, fives, tens, and twenties in ascending order.
                       Ex. payment = [3, 0, 2, 1, 0, 0, 1] refers to 3 pennies, 2 quarters, 1 one-dollar bill, and 1 twenty-dollar bill.
        @return cents An integer value that represents the total value of currency specified in the input list.
    '''
    ## @brief A tuple representing the value of each currency denomination in cents
    amounts = (1, 5, 10, 25, 100, 500, 1000, 2000)
    ## @brief Holds the sum of the total payment
    cents = 0
        
    for i in range(len(payment)):
        cents += (payment[i] * amounts[i]) 
    
    return cents
 
if __name__ == '__main__':
    ## Initialize code to run FSM (not the init state). This means building the iterator as generator object
    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
