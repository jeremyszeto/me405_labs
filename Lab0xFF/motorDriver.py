'''
@file  motorDriver.py
@brief  This file contains a motor driver class for a DRV8847 motor.
@details  This file contains a  motor driver class that is used to drive a motor
          from the Nucleo board. The driver implements bidirectional PWM control
          at a user-specified frequency. The motor can be set to one of four states as specified in the DRV8847 reference manual: Sleep (disabled), Coast (enabled && duty = 0), Reverse (duty < 0), or Forward (duty > 0).
          
@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
'''
import pyb, micropython

## An emergency buffer to store errors that are thrown inside ISR.
micropython.alloc_emergency_exception_buf(200)
        
class MotorDriver:
    '''
    @brief   This motor driver class implements a motor driver for the Nucleo MCU.
    @details This motor driver class implements bidirectional PWM control for the motors on the ball balancing platform. The motor can be enabled, disabled, or set to a PWM duty cycle between -100 and 100 to control speed and direction.
    '''
    def __init__(self, pin_nSLEEP, pin_nFAULT, IN1, IN2, timer):
        '''
        @brief Initializes the pins and timer for a MotorDriver object.
        @param pin_nSLEEP  The sleep pin on the DRV8847 motor driver chip.
        @param pin_nFAULT  The fault detection pin on the DRV8847 motor driver chip.
        @param IN1 Input to H-bridge 1.
        @param IN2 Input to H-bridge 2.
        @param timer   A pyb.Timer object for PWM generation on IN1 and IN2.
        '''
        ## The sleep pin on the DRV8847 motor driver chip.
        self.pin_nSLEEP = pin_nSLEEP
        self.pin_nSLEEP.low() # Disable upon startup.
        
        ## The fault detection pin on the DRV8847 motor driver chip.
        self.pin_nFAULT = pin_nFAULT
                
        ## The timer channel used to control IN1.
        self.timer_ch1 = timer.channel(IN1['Channel'], pyb.Timer.PWM, pin=IN1['Pin'])
        
        ## The timer channel used to control IN2.
        self.timer_ch2 = timer.channel(IN2['Channel'], pyb.Timer.PWM, pin=IN2['Pin'])
        
        ## A boolean representing whether a fault is currently detected.
        self.fault = False
        
        ## The external interrupt triggered when by the nFAULT pin.
        self.extint = pyb.ExtInt(self.pin_nFAULT, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.callback)
            
    def enable(self):
        '''@brief  Enables the motor.
        '''
        self.pin_nSLEEP.high() # Raise the sleep mode pin
    
    def disable(self):
        '''@brief  Disables the motor and sets the PWM duty cycle to 0.
        '''
        self.pin_nSLEEP.low() # Lower the sleep mode pin
        self.set_duty(0)      # Turn off signals for redundancy
    
    def set_duty(self, duty):       
        '''
        @brief Sets the motor duty cycle.
        @details Positive values cause actuation in one direction, negative values in the opposite direction.
        @param duty  A signed integer holding the duty cycle of the PWM signal sent to the motor
        '''
        if self.fault:
            self.check_fault()
        elif (abs(duty) > 100):
            if duty < 0:
                duty = -100
            elif duty > 0:
                duty = 100
        elif duty == 0: # Motor Coast
            self.timer_ch1.pulse_width_percent(0)
            self.timer_ch2.pulse_width_percent(0)
        elif duty > 0: # Motor Forward
            self.timer_ch1.pulse_width_percent(duty)
            self.timer_ch2.pulse_width_percent(0)
        elif duty < 0: # Motor Reverse 
            self.timer_ch1.pulse_width_percent(0)
            self.timer_ch2.pulse_width_percent(abs(duty))
            
    def check_fault(self):
        '''
        @brief Checks the fault state of the motor.
        '''
        if self.pin_nFAULT.value() == 1:
            self.fault = False
            self.enable()
            print('Fault cleared.\n')
            
        else:
            input('''Fault detected. The motor will remain disabled until the issue is fixed and the user presses ENTER.\n''')
            self.enable()
            while self.pin_nFAULT.value() == 0:
                input('Fault still active. Check the issue again, then press ENTER.\n')
                self.disable()
                self.enable()
            
    def callback(self, IRQ):
        ''' 
        @brief   An external interupt callback function.
        @details An interrupt handler called when the nFAULT pin goes low.
        @param IRQ The pin that triggers the IRQ (not used).
        '''
        self.disable()
        self.fault = True
# Test code.
if __name__ == '__main__':   
    ## Motor nSLEEP pin used to enable the driver chip.
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    ## Motor nFAULT pin used for fault detection.
    pin_nFAULT = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
    ## A timer instance for the motors using Timer3 @ 20kHz.
    tim3 = pyb.Timer(3, freq=20000)

    ## Pin address and channel number for the M1- output pin.
    M1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B4), 'Channel': 1 }
    ## Pin address and channel number for the M1+ output pin.
    M1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B5), 'Channel': 2 }
    ## A MotorDriver object used to control Motor 1.
    M1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3)

    ## Pin address and channel number for the M2- output pin.
    M2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B0), 'Channel': 3 }
    ## Pin address and channel number for the M2+ output pin.
    M2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B1), 'Channel': 4 }
    ## A MotorDriver object used to control Motor 2.
    M2 = MotorDriver(pin_nSLEEP, pin_nFAULT, M2_CH1, M2_CH2, tim3)

    # Enable both motors
    M1.enable()
    M2.enable()
    
    try:
        # Run forever
        while True:
            ## user input duty cycle for testing
            duty = input('Duty cycle for Motor 1: ')
            M1.set_duty(int(duty))
            pyb.delay(500)
            M1.disable()
            
            duty = input('Duty cycle for Motor 2: ')
            M2.set_duty(int(duty))  
            pyb.delay(500)
            M2.disable()

            # test faults
            M1.set_duty(-35)
            pyb.delay(250)
            
    # Exit condition for errors or CTRL+C       
    except:
        M1.disable()
        M2.disable()
        print("Exiting Program")
