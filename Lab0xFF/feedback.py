'''
@file  feedback.py
@brief  This file contains a closed-loop state controller class.

@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
'''
class Feedback:
    '''
    @brief Utilizes full state feedback control to calculate the necessary PWM level  to apply to a motor.
    '''
    def __init__(self, k_m, k_ctrl):
        '''
        @brief Constructs a Feedback object.
        @param k_m A float representing the motor gain.
        @param k_ctrl A 4x1 gain matrix containing the control system gains used for full-state feedback control.
        '''           
        ## A float representing the motor gain.
        self.k_m = k_m
        ## Gain matrix
        self.k_ctrl = k_ctrl
        
    def update(self, pos_ball, pos_platform, vel_ball, vel_platform):
        ''' 
        @brief              Calculate the PWM level to send to a motor to apply a corrective torque to the system.
        @param pos_ball     Position of the ball [m] from the center of the platform.
        @param pos_platform Position of the platform from equillibrium [rad].
        @param vel_ball     Velocity of the ball [m/s].
        @param vel_platform Velocity of the platform [rad/s].
        @return level       Duty level to be applied to the motor.
        '''
        
        ## The total duty level applied to the motor.
        level = (-1)*(self.k_ctrl[0]*pos_ball + self.k_ctrl[1]*pos_platform + self.k_ctrl[2]*vel_ball + self.k_ctrl[3]*vel_platform) * self.k_m
        return level

        