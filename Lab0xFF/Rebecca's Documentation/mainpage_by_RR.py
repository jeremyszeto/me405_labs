
'''
@file mainpage_RR.py
@brief 
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date March 7, 2021
'''

#  @section sec_LabSeven Lab 0x0FF Term Project
#  
#  @subsection sec_t1 Resistive Touch Screen Hardware
#  This project sets up the resistive touch screen that detects the position of the ball on the platform.
#  The resistive touch panel driver makes it possible to track the planar position of the ball on the platform. The screen is 176.00 mm x 99.36 mm; however, 
#  the active measurement area is not the same as the screen area. Figure 1, taken from the 
#  lab manual, is a schematic representation of a 4-wire resistive touch panel. 
#  @image html res_touchPanel.PNG  "Figure 1. 4-Wire Resistive Touch Panel Schematic Representation"
#  The screen driver scans the x and y position of the ball, as well as whether or not the ball is in contact with the screen. 
#  There is a delay of 4 microseconds between each x-, y-, and z-scan, since it takes approximately 3.6 microseconds for the ADC count reading to settle.
#  The screen driver was able to read from a single channel in less than 500 microseconds as well as read all three channels in less than 1500 microseconds.
#  This is critical for the driver to work cooperatively with other code. The results from Putty are shown below:
#  \code{.py}
#  execfile('TouchScreenDriver.py')
#  x-Scan Time [microseconds]: 500, x-value [cm]: 5.560221
#  y-Scan Time [microseconds]: 401, y-value: [cm] 8.715
#  z-Scan Time [microseconds]: 376, Point of contact? True
#  xyz-Scan Time [microseconds]: 1143, x: [cm] 5.049171, y: [cm] 8.155, Point of contact? True                                                                                                                                                                                                      
#  \endcode
#  All three channels were able to be polled in under 1500 microseconds as shown above with 'xyz-Scan'; however, the time was further reduced to 
#  908 microseconds by optimizing the number of pin configurations. The source code may be found at: https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/Rebecca's%20Drivers/TouchScreenDriver_by_RR.py
#  The output in Putty shown below shows a further decrease in scanning time for all three channels with the 'quick-Scan' method:
#  \code{.py}
#  quick-Scan Time [microseconds]: 908, x: [cm] 4.865193, y: [cm] 8.225, Point of contact? True           
#  \endcode
#   A surface level was used to ensure the platform was level before
#  taking any measurements. Then the calibration of the touch screen was verified by marking the touch screen with tape and lightly placing the tape on the screen as pictured below.
#  @image html cal_screen.jpg  "Figure 2. Calibration Verification of the Resistive Touch Screen"
#  @subsection sec_t2 Driving DC Motors with PWM
#  The balancing platform utilizes two DC motors (one motor controls each planar axis). The driver is written for the DRV8847 dual H-bridge. The encoders attached to the motors could be used
#  to determine the angular velocity of the platform about each rotational axis; however, for conciseness it was decided to use the IMU readings instead. The motor driver then
#  just takes in a single duty cycle value that relates to the torque applied to the platform. Furthermore, a safety fault detection was also implemented to stop the motor when a fault has been detected.
#  Testing showed that the duty cycle to overcome static friction on each motor was a duty cycle of at least 40%. In the final project, the motor arm was attached to the platform as shown in Figure 3. 
#  @image html motorArmMount.jpg  "Figure 3. DC Motor and Arm Placement"
#  See @ref motorDriver.py for the file documentation. In addition, the source code may be found at: https://bitbucket.org/jeremyszeto/me405_labs/src/main/Lab0xFF/Rebecca's%20Drivers/motorDriver_by_RR.py.
#  @subsection sec_t3 Reading from Inertial Measurement Unit
#  The IMU (BNO055) was soldered to one corner of the platfrom and powered with 5VDC. The sensor communicates using I2C protocol so there are two pins required for communication. The pins are the SCL (clock) and SDA (data) pins and 
#  wiring is as shown in Figure 4. 
#  @image html imu.PNG  "Figure 4. IMU Setup and Placement on Platform"
#  The IMU is able to determine the orientation of the platform as well as the angular velocity of the platform. The sensor outputs are necessary for feedback control for the balancing platform. 
#  Fortunately, a driver was found for the BNO055 IMU written in micropython. The driver includes the following methods: a method to change the operating mode of the IMU, a method to retrieve the calibration status, and
#  a method to read the Euler angles. The Euler angles are output in degrees in the form of (heading, roll, pitch) as shown in Figure 5. 
#  @image html EulerAng.PNG  "Figure 5. Euler Angles Reference"
#  Calibration of the IMU is as simple as stabilizing the platform in a set position for a few seconds to calibrate the gyroscope. Calibration was complete once the calibration status method returned a value of 3.
#  The angular velocity was obtained from recording the change in Euler angles with respect to time. This driver was incorporated in the final design to obtain readings of the platform's roll and pitch, which was necessary for the
#  closed loop control of the balancing platform.
#  Please see https://github.com/micropython-IMU/micropython-bno055#readme for the reference of the BNO055 driver.
#  @subsection sec_t4 Balancing the Ball
#  In the last step of the project all hardware, drivers, and proportional controller were combined to form the closed-loop control system for the balancing ball on the platform. The position and velocity of the ball 
#  is obtained from the touch panel, while the angle and anglular velocity of the platform is obtained from the IMU. One motor applies a torque about the x-axis, while the other motor applies a torque about the y-axis. The torque required to balance the platform is related to the duty cycle by: 
#  @image html duty_eq.gif
#  Where the matrix for gains is expressed as:
#  @image html gains.gif
#  The matrix for the state variables is:
#  @image html state_var.gif
#  And the parameters obtained from the DC motor datasheet are:
#  @image html param.gif
#  In summary, a torque about the x-axis rotates the platform an amount theta_y which in turn moves the ball along the x-axis. The gains were previously calculated to be XXXX; however, the tuned values 
#  were XXXX.
#   A demonstration of the project running is shown here: XXXX. 
#  
#  TO DO for the video: describe the control scheme proportional control for closed feedback, state initial conditions, demonstrate ball balancing
#  
#    
#  @author Rebecca Rodriguez and Jeremy Szeto
#  @date June 11, 2021