'''
@file  pid.py
@brief   Contains a class PID that controls the speed of a motor.
@details This class uses Proportional-Integral-Derivative control to correct the speed of a motor.
@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
'''
class PID:
    '''
    @brief  Uses Proportional-Integral-Derivative control to control the speed of a motor. This class is used in the term project to level out the balance platform.
    '''
    def __init__(self, Kp, Ki, Kd):
        '''
        @brief Constructs a PID object.
        @param Kp Proportional gain.
        @param Ki Integral gain.
        @param Kd Derivative gain.
        '''
        ## The gain value for proportional control.
        self.Kp = Kp
        ## The gain value for integral control.
        self.Ki = Ki
        ## The gain value for derivative control.
        self.Kd = Kd
        ## The error between measured and desired motor speed.
        self.err = 0
        ## The motor speed error from the last iteration.
        self.err_prev = 0
        ## The proportional error from Kp gain.
        self.P_err = 0
        ## The integral error from Ki gain.
        self.I_err = 0
        ## The derivative error from Kp gain.
        self.D_err = 0
        ## The level (%/rpm) sent to the motor driver.
        self.level = 0
        
    def update(self, W_ref, W_meas):
        '''
        @brief             Runs one iteration of PID control.
        @param W_ref       The desired motor speed.
        @param W_meas      The actual speed of the motor measured from an encoder.
        @return level      The duty cycle that should be sent to the motor
                           to correct for the closed-loop error.
        '''   
        
        self.err = W_ref - W_meas # Calculates the error
        self.P_err = self.Kp * self.err
        self.I_err += (self.Ki * self.err)
        self.D_err = self.Kd * (self.err - self.err_prev)
        self.level = self.P_err + self.I_err + self.D_err
        self.err_prev = self.err # Updates the previous error (derivative)
        return self.level