'''
@file main.py
@brief The main script that runs the closed-loop control system of a 2DoF ball 
balancing platform.
@details This file initializes each system component (two motors and encoders, 
resistive touch panel, IMU, and feedback controller for each motor) that are 
controlled via a finite state machine in taskController.py.

@package Lab0xFF
@brief This package contains main.py, touchPanelDriver.py, motorDriver.py, encoderDriver.py, feedback.py, pid.py, and controllerTask.py
         
@author Jeremy Szeto and Rebecca Rodriguez
@date June 10, 2021
'''
import pyb, machine
from touchPanelDriver import touchPanelDriver
from motorDriver import MotorDriver
from encoderDriver import EncoderDriver
from bno055 import BNO055
from feedback import Feedback
from controllerTask import ControllerTask

print('Initializing touch panel..')
## Calibration distance in mm between two points on the x axis.
x_len        = (-20,  20)
## Calibration count between two points on the x axis.
x_count      = (1639, 2447)
## Calibration distance in mm between two points on the y axis.
y_len        = (-20,  20)
## Calibration count between two points on the y axis.
y_count      = (1307, 2569)
## Calibration coordinate at the center of the touch panel.
center = (2036, 2045) 

## touchPanelDriver object used control the touch panel.
tp = touchPanelDriver(pyb.Pin.board.PA7, 
                    pyb.Pin.board.PA1, 
                    pyb.Pin.board.PA0, 
                    pyb.Pin.board.PA6, 
                    x_len, x_count, y_len, y_count, center)

print('Initializing motors..')
## A timer instance for the motors using Timer3.
tim3 = pyb.Timer(3, freq=20000)
## MotorDriver object used to control Motor 1.
M1 = MotorDriver(pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP), 
    pyb.Pin(pyb.Pin.board.PF13, pyb.Pin.IN), 
    {'Pin': pyb.Pin(pyb.Pin.cpu.B4), 'Channel': 1}, 
    {'Pin': pyb.Pin(pyb.Pin.cpu.B5), 'Channel': 2}, 
    tim3)

##MotorDriver object used to control Motor 2.
M2 = MotorDriver(pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP), 
    pyb.Pin(pyb.Pin.board.PF13, pyb.Pin.IN),
    {'Pin': pyb.Pin(pyb.Pin.cpu.B0), 'Channel': 3}, 
    {'Pin': pyb.Pin(pyb.Pin.cpu.B1), 'Channel': 4}, 
    tim3)

# Enable both motors
M1.enable()
M2.enable()

print('Initializing encoders..')
## A timer instance for Encoder 1 using Timer4 with a period of 65535.
tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
## EncoderDriver object used to control Encoder 1.
E1 = EncoderDriver({'Pin': pyb.Pin(pyb.Pin.cpu.B6), 'Channel': 1}, 
                {'Pin': pyb.Pin(pyb.Pin.cpu.B7), 'Channel': 2}, 
                tim4)

## A timer instance for Encoder 2 using Timer8 with a period of 65535.
tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
## EncoderDriver object used to control Encoder 2.
E2 = EncoderDriver({'Pin': pyb.Pin(pyb.Pin.cpu.C6), 'Channel': 1}, 
                {'Pin': pyb.Pin(pyb.Pin.cpu.C7), 'Channel': 2}, 
                tim8)

print('Initializing IMU..')
## I2C channel on the Nucleo.
i2c = machine.I2C(1)
## IMU object used to control the IMU.
imu = BNO055(i2c)

print('Initializing State Feedback Controller..')
## Gain applied to the ball position [N].
k1 = -203
## Gain applied to the platform position [N*m].
k2 = -29
## Gain applied to the ball velocity [N*s].
k3 = -42
## Gain applied to the platform velocity [N*m*s].
k4 = -2.25

## Motor gain.
k_m = 2.21*1000/(12*13.8)

## Gain matrix.
k_ctrl = [k1, k2, k3, k4]

## Feedback controller object for Motor 1.
F1 = Feedback(k_m, k_ctrl)
## Feedback controller object for Motor 2.
F2 = Feedback(k_m, k_ctrl)
## Time in ms between iterations of the TaskController FSM.
interval = 12
## The controller for the entire system.
system = ControllerTask(M1, M2, E1, E2, F1, F2, tp, imu, interval)

if __name__ == '__main__':   
    # Run the system continuously.
    system.run()