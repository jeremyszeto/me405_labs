'''
@file touchPanelDriver.py
@brief This file contains a driver for a resistive touch panel.
@details A hardware driver for the resistive touch panel. Ultimately, the touch panel will be used to indicate the planar position of the ball on the platform for the final project. The driver makes it possible to independently read the x, y, or z position. Here the z-component tells whether or not contact has been made with the screen. Settling time was taken into consideration when designing the methods and it is possible to read from a single  channel in less than 500 microseconds and all three channels in less than 1500 microseconds. The resulting x, y, and boolean value for z is returned as a tuple. The strict timing requirement is required for the incorporation of this code with the rest of the project.

@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
''' 
from pyb import ADC, Pin, micros, udelay, delay

class touchPanelDriver:
    '''
    @brief    A driver class for a resistive touch panel.
    @details  This class contains an constructor used to define and calibrate a
              touchPanel object, methods to measure the raw horizontal and vertical contact readings and a method to indicate if there is contact with the panel.
    '''
    def __init__(self, x_p_pin, x_m_pin, y_p_pin, y_m_pin, x_len, x_count, y_len, y_count, center):
        '''
        @brief The constructor for the touch panel driver.
        @param x_p_pin    The x_p pin on the touch panel.
        @param x_m_pin    The x_m pin on the touch panel.
        @param y_p_pin    The y_p pin on the touch panel.
        @param y_m_pin    The y_m pin on the touch panel.
        @param x_len  Calibration distance in mm between two points on the x axis.
        @param x_count  Calibration count between two points on the x axis.
        @param y_len  Calibration distance in mm between two points on the y axis.
        @param y_count  Calibration count between two points on the y axis.
        @param center Calibration coordinate at the center of the touch panel.
        '''
        ## The x_p pin on the touch panel.
        self.x_p = x_p_pin
        ## The x_m pin on the touch panel.
        self.x_m = x_m_pin
        ## The y_p pin on the touch panel.
        self.y_p = y_p_pin
        ## The y_m pin on the touch panel.
        self.y_m = y_m_pin
        
        ## The pyb.ADC object associated with the x_m pin, used to measure the Y position.
        self.ADC_x = ADC(self.x_m)
        ## The pyb.ADC object associated with the y_m pin, used to measure the X position.
        self.ADC_y = ADC(self.y_m)
        
        ## Reference positions for the calibration of the x-component (min, max).
        self.x_len = x_len
        ## Reference counts for the calibration of the x-component (min, max).
        self.x_count = x_count
        ## Reference positions for the calibration of the y-component (min, max).
        self.y_len = y_len
        ## Reference counts for the calibration of the y-component (min, max).
        self.y_count = y_count
        ## Counts at the center of the panel for the calibration of the center point (X, Y).
        self.center = center

        ## The last scanned component.
        self.last_scan = ''
        ## The current x position
        self.x_pos = 0
        ## The previous x position
        self.x_pos_old = 0
        ## The difference between x_pos and x_pos_old
        self.delta_x = 0
        ## The current y position
        self.y_pos = 0
        ## The previous y position
        self.y_pos_old = 0
        ## The difference between y_pos and y_pos_old
        self.delta_y = 0
        
    def scan_x(self):
        '''
        @brief   Scans the X component of the touch panel.
        @details Scans the X position along the resistive touch panel by energizing
                 the resistor divider between x_p and x_m, floating y_p, and measuring the ADC at y_m. Then, the count is converted to distance (mm) from the center point.
        @return x_pos A float representing the horizontal displacement (mm) of the point of contact from the center of the touch panel.
        '''
        if self.last_scan == 'z':
            self.x_p.init(mode=Pin.OUT_PP, value=1)
            self.y_p.init(mode=Pin.IN)
        
        # Must reinitialize each pin
        else:
            self.x_p.init(mode=Pin.OUT_PP, value=1)
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            self.y_p.init(mode=Pin.IN)
            self.ADC_y = ADC(self.y_m)
        
        self.last_scan = 'x' # Indicate that this was the last component scanned
        
        udelay(4) # Wait for signal to settle
        x_pos = (self.center[0]-self.ADC_y.read())*(self.x_len[1]-self.x_len[0])/(self.x_count[1]-self.x_count[0])
        return x_pos
    
    def scan_y(self):
        '''
        @brief   Scans the Y component of the touch panel.
        @details Scans the Y position along the resistive touch panel by energizing
                 the resistor divider between y_p and y_m, floating x_p, and measuring the
                 ADC at x_m. Then, the count is converted to distance (mm) from the center point.
        @return y_pos A float representing the vertical displacement (mm) of the
                           point of contact from the center of the touch panel.
        '''
        if self.last_scan == 'z':
            # No change to x_p
            self.ADC_x = ADC(self.x_m)
            # No change to y_p
            self.y_m.init(mode=Pin.OUT_PP, value=1)
         
        # Must (re)initialize each pin
        else:
            self.x_p.init(mode=Pin.IN)
            self.ADC_x = ADC(self.x_m)
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            self.y_m.init(mode=Pin.OUT_PP, value=0)
        
        
        self.last_scan = 'y' # Indicate that this was the last component scanned
        
        udelay(4) # Wait for signal to settle
        y_pos = (self.center[1]-self.ADC_x.read())*(self.y_len[1]-self.y_len[0])/(self.y_count[1]-self.y_count[0])
        return y_pos
    
    def scan_z(self):
        '''
        @brief   Scans the Z component of the RTP.
        @details Scans the resistive touch panel to detect if there is contact with the screen by energizing the resistor divider between y_p and x_m, floating x_p, and measuring the ADC at y_m. A count measurement of 4000 or more indicates that there is no contact.
        @return contact A boolean value representing whether there is contact with the RTP.
        '''
        # Checks if the last component scanned was 'X'
        if self.last_scan == 'x':
            self.x_p.init(mode=Pin.IN)
            # No change to x_m
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            # No change to y_m
        
        # Checks if the last component scanned was 'Y'
        elif self.last_scan == 'y':
            # No change to x_p
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            # No change to y_p
            self.ADC_y = ADC(self.y_m)
        
        # Must (re)initialize each pin
        else:
            self.x_p.init(mode=Pin.IN)
            self.x_m.init(mode=Pin.OUT_PP, value=0)
            self.y_p.init(mode=Pin.OUT_PP, value=1)
            self.ADC_y = ADC(self.y_m)
        
        self.last_scan = 'z' # Indicate that this was the last component scanned
                
        udelay(4) # Wait for signal to settle
        
        ## A boolean representing whether there is contact with the panel.
        contact = self.ADC_y.read() < 4000
        return contact
    
    def scan_rtp(self):
        '''
        @brief Scans the X, Y, and Z components of the RTP
        @details Scans the horizontal and vertical displacement (mm) from the center point
                 of the resistive touch panel, as well as whether there is contact with the panel. If there is no contact with the touch panel, then returns "None" for both displacements.
        @return A tuple representing the x-position (mm), y-position (mm), and state of contact (bool) with the RTP.
        '''
        # Measure and record if there is contact with the RTP.
        contact = self.scan_z()
        
        # Contact detected
        if contact:
            # Measure and record the horizontal/vertical displacements
            x_pos = self.scan_x()
            y_pos = self.scan_y()
            return (x_pos, y_pos, contact)
        
        # No contact detected, state of system is known (save time)
        else:
            return (None, None, False)
        
    def scan_rtp_fast(self):
        '''
        @brief See scan_rtp().
        @details An optimized version of scan_rtp() that scans the X, Y, and Z components
                 of the touch panel in a more efficient manner by removing calls to other class methods, minimimizing the number of pin configurations and scanning the components in a better sequence.
        @return A tuple representing the x-position (mm), y-position (mm), and state of contact (bool) with the RTP.
        '''

        # (re)Initialize every pin to scan for 'X' component
        self.x_p.init(mode=Pin.OUT_PP, value=1)
        self.x_m.init(mode=Pin.OUT_PP, value=0)
        self.y_p.init(mode=Pin.IN)
        self.ADC_y = ADC(self.y_m)
        
        self.x_pos_old = self.x_pos
        # Measure and record the horizontal position (mm)
        self.x_pos = (self.center[0]-self.ADC_y.read())*(self.x_len[1]-self.x_len[0])/(self.x_count[1]-self.x_count[0])
        
        # Initialize pins to scan for 'Z' component
        self.x_p.init(mode=Pin.IN)
        self.y_p.init(mode=Pin.OUT_PP, value=1)
        
        # Scan for contact with the RTP.
        contact = self.ADC_y.read() < 4000
        
        # No contact detected, state of system is known (save time)
        if not contact:
            return (0, 0, False)
        
        # Contact detected
        else:
            # Initialize pins to scan for 'Y' component
            self.ADC_x = ADC(self.x_m)
            self.y_m.init(mode=Pin.OUT_PP, value=0)
            
            self.y_pos_old = self.y_pos
            # Measure and record the vertical position (mm)
            self.y_pos = (self.center[1]-self.ADC_x.read())*(self.y_len[1]-self.y_len[0])/(self.y_count[1]-self.y_count[0])
            
            # Return that measured components
            return (self.x_pos, self.y_pos, contact)
        
# Test code.
if __name__ == '__main__':   

    ## Calibration distance in mm between two points on the x axis.
    x_len        = (-20,  20)
    ## Calibration count between two points on the x axis.
    x_count      = (1639, 2447)
    ## Calibration distance in mm between two points on the y axis.
    y_len        = (-20,  20)
    ## Calibration count between two points on the y axis.
    y_count      = (1307, 2569)
    ## Calibration coordinate at the center of the touch panel.
    center = (2036, 2045) 
    
    ## Corresponds with the x_p pin on the resistive touch panel.
    PA7 = pyb.Pin.board.PA7
    ## Corresponds with the x_m lead on the resistive touch panel.
    PA1 = pyb.Pin.board.PA1
    ## Corresponds with the y_p lead on the resistive touch panel.
    PA0 = pyb.Pin.board.PA0
    ## Corresponds with the y_m lead on the resistive touch panel.
    PA6 = pyb.Pin.board.PA6

    ## RTP_Driver object.
    task = touchPanelDriver(PA7, PA1, PA6, PA0, x_len, x_count, y_len, y_count, center)
    
    # Continuously print position
    while True:
        # Scan for components and get rough estimate of latency
        ## start time in ms
        start = micros()
        ## holds coordinates in a tuple
        data = task.scan_rtp_fast()
        ## end time in ms
        end = micros()
        
        if data[2]:
            print('{:.1f},{:.1f}'.format(data[0], data[1]))
        else:
            print('No contact')

        print('Scan Time: '+ str(start-end)+'\n')
        delay(100)