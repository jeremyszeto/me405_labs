'''
@file encoderDriver.py
@brief This file contains a driver for a quadrature encoder.
@details The driver is initialized with a timer and corresponding timer channels and Nucleo pins. It measures encoder position and corrects for under/overflow.
@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
'''
import pyb

class EncoderDriver():
    '''
    @brief      Uses a timer instance to read from a quadrature encoder.
    @details    This class initializes the pins, timer, and timer
                channels for a quadrature encoder. Objects of this class read 
                the position of the encoder from two optical sensors and 
                compensates for under/overflow. A run task is created to
                constantly update the encoder position and delta at a regular
                interval to avoid missing encoder ticks.
    '''
    def __init__(self, IN1, IN2, timer):
        '''
        @brief Constructs an EncoderDriver object.
        @param IN1 First signal pin for the optical sensor on the encoder.
        @param IN2 Second signal pin for the optical sensor on the encoder.
        @param timer A timer object with prescalar and period used to construct
         timer channels for each pin.
        '''
        ## The timer used to measure encoder counts.
        self.timer = timer
        ## Timer channel for pin IN1.
        self.timer_ch1 = self.timer.channel(IN1['Channel'], pyb.Timer.ENC_AB, pin=IN1['Pin'])
        ## Timer channel for pin IN2.
        self.timer_ch2 = self.timer.channel(IN2['Channel'], pyb.Timer.ENC_AB, pin=IN2['Pin'])
        ## Current encoder position.
        self.current_tick = 0
        ## Previous encoder position.
        self.position_old = 0
        ## Newest encoder position.
        self.position_new = 0
        ## Difference between position_new and position_old.
        self.delta = self.position_new - self.position_old
        
    def update(self):
        ''' @brief Corrects under/overflow for the encounter count and updates delta, 
                   position_old, and position_new
        '''           
        self.delta = self.timer.counter() - (self.position_new)
        if self.delta > 0xFFFF/2: # Check for underflow
            self.delta -= (0xFFFF + 1)
            
        elif self.delta < -0xFFFF/2: # Check for overflow
            self.delta += (0xFFFF + 1)
        
        self.position_old = self.position_new
        self.position_new  = self.position_old + self.delta

    def get_position(self):
        '''
        @brief   Returns the absolute value of the encoder position accounting for over/underflow.
        @return  The most recent value of the encoder position.
        '''
        return self.position_new
    
    def get_old_position(self):
        '''
        @brief   Returns the previous absolute value of the encoder position accounting for over/underflow.
        @return  The previous value encoder position.
        '''
        return self.position_old
    
    def set_position(self, setValue):  
        '''
        @brief          Sets all forms of the current encoder position to a specified value.
        @param setValue An integer value for the encoder position (count) to be set to.
                        By default, setValue is 0 in order to zero the encoder.
        ''' 
        self.position_old = setValue 
        self.position_new = setValue
        self.timer.counter(setValue)
    
    def get_delta(self):
        '''
        @brief      Updates and returns the difference in encoder position.
        @return     The difference between position_new and position_old.
        '''
        return self.delta

# Test code
if __name__ == '__main__':
    ## A timer instance for Encoder 1 using Timer4 with a period of 65535.
    tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
    ## An EncoderDriver object used to control Encoder 1.
    E1 = EncoderDriver({'Pin': pyb.Pin(pyb.Pin.cpu.B6), 'Channel': 1}, 
                    {'Pin': pyb.Pin(pyb.Pin.cpu.B7), 'Channel': 2}, 
                    tim4)

    ## A timer instance for Encoder 2 using Timer8 with a period of 65535.
    tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
    ## An EncoderDriver object used to control Encoder 2.
    E2 = EncoderDriver({'Pin': pyb.Pin(pyb.Pin.cpu.C6), 'Channel': 1}, 
                    {'Pin': pyb.Pin(pyb.Pin.cpu.C7), 'Channel': 2}, 
                    tim8)


    ## Time in milliseconds between measuring encoder data.
    interval = 20
    ## Start time in ms.
    start_time = pyb.millis()
    ## Next time to run the task.
    next_time = interval + pyb.elapsed_millis(start_time) 
    ## Measured RPM of Encoder 1.
    W1 = 0
    ## Measured RPM of Encoder 2.
    W2 = 0

    # Run forever
    while True:
        # Check for next run time.
        if (pyb.elapsed_millis(start_time) >= next_time):
            # Update the encoder positions/deltas
            E1.update()
            E2.update()
            W1 = E1.get_delta() * 10**3 * 60 / (interval * 4000) # Calculate rpm
            print("E1 RPM = " + str(W1))
            W2 = E2.get_delta() * 10**3 * 60 / (interval * 4000) # Calculate rpm
            print("E2 RPM = " + str(W2))
            next_time += interval # Update the next runtime