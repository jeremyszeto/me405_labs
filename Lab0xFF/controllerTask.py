'''
@file controllerTask.py
@brief  This file contains a finite-state machine class that schedules tasks.
@details This class contains a finite-state machine that schedules tasks to control a 2DoF ball balancing system. The FSM runs on a specified interval that repetitively calls tasks to balance the ball.

@package Lab0xFF
@author Jeremy Szeto
@date June 10, 2021
'''
import pyb
from pid import PID

class ControllerTask:
    '''
    @brief A full state feedback controller.
    
    @details This class contains a finite-state machine that schedules tasks to control a 2DoF ball balancing system. The FSM runs on a specified interval that repetitively calls tasks which measure encoder values, touch panel values, IMU values, and sets the PWM level of motors.
    '''
    ## State 0: Initialization.
    S0_INIT = 0
    ## STATE 1: Calibration.
    S1_CALIBRATION = 1
    ## STATE 2: Control (Platform).
    S2_MEASURE = 2
    ## STATE 3: Control (Platform + Ball).
    S3_CONTROL = 3
    ## Length of the platform [m].
    l_p = 110e-3
    ## Length of the motor arm [m].
    r_m = 60e-3

    def __init__(self, M1, M2, E1, E2, F1, F2, RTP, IMU, interval):
        '''
        @brief Constructs a TaskController object. 
        @param M1        A MotorDriver object used to control Motor 1.
        @param M2        A MotorDriver object used to control Motor 2.
        @param E1        An EncoderDriver object used to control Encoder 1.
        @param E2        An EncoderDriver object used to control Encoder 2.
        @param F1 A Feedback controller object used to calculate the PWM level of Motor 1 with full-state feedback.
        @param F2 A Feedback controller object used to calculate the PWM level of Motor 2 with full-state feedback.
        @param RTP         A touchPanelDriver object used to measure the contact point on the resistive touch panel.
        @param IMU         An IMU object used to measure the orientation of the platform during calibration.
        @param interval    An integer representing the time in ms between iterations of the TaskController FSM.
        '''                     
        ## MotorDriver object used to control Motor 1.
        self.M2 = M1
        ## MotorDriver object used to control Motor 2.
        self.M1 = M2
        ## EncoderDriver object used to control Encoder 1.
        self.E2 = E1
        ## EncoderDriver object used to control Encoder 2.
        self.E1 = E2
        ## touchPanelDriver object used to measure the contact point on the resistive touch panel.
        self.RTP = RTP
        ## IMU object used to measure the orientation of the IMU.
        self.IMU = IMU
        ## Feedback object used to implement state feedback control for Motor/Encoder 1.
        self.F1 = F1
        ## Feedback object used to implement state feedback control for Motor/Encoder 2.
        self.F2 = F2
        ## Time in milliseconds between runs of the task. 
        self.interval = interval    
        ## Boolean value representing if Encoder 1 has been calibrated.
        self.E1_calibrated = False
        ## Boolean value representing if Encoder 2 has been calibrated.
        self.E2_calibrated = False
        ## x displacement of the ball [m].
        self.x = 0
        ## y displacement of the ball [m].
        self.y = 0
        ## x velocity of the ball [m/s].
        self.x_dot = 0
        ## y velocity of the ball [m/s].
        self.y_dot = 0
        ## The current state of the FSM.
        self.state = S0_INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the FSM task.
        @details The TaskController FSM can be one of four various states: Initialization,
                 Calibration, Control of the Platform, or Control of the Ball. Upon powering up, the system immediately enters S0 - Initialization - where system components are initialized. After one iteration, the FSM moves into
                 a calibration state, which zeros each encoder at a level position determined by the IMU. Once both encoders are calibrated, the system controls the platform or the platform and ball together if the touch panel senses contact.
        '''
        ## The current time in milliseconds.
        self.current_time = pyb.millis()
        ## The time for when the task should run next.
        self.next_time = self.current_time + self.interval
        try:
            # Run forever
            with open ('data.csv', 'w') as fp:  
                while True:
                    self.current_time = pyb.millis() # Update the current time
                    # Check to see if the time exceeded the interval
                    if (self.current_time >= self.next_time):  
                        if(self.state == self.S0_INIT):
                            print('Ready.\n\n')
                            self.M1.enable() # Enable both motors.
                            self.M2.enable()
                            pyb.delay(1000) # Brief delay to allow IMU initialization
                            ## The angular position of the platform about the x-axis [rad].
                            self.theta_x = 0
                            ## The angular velocity of the platform about the x-axis [rad/s].
                            self.theta_dot_x = 0
                            ## The angular position of the platform about the y-axis [rad].
                            self.theta_y = 0
                            ## The angular velocity of the platform about the y-axis [rad/s].
                            self.theta_dot_y = 0
                            ## The duty applied to the x-axis motor.
                            self.T_x = 0
                            ## The duty applied to the y-axis motor.
                            self.T_y = 0
                            ## The last x displacement of the ball [m].
                            self.x_old = 0
                            ## The last y displacement of the ball [m].
                            self.y_old = 0
                            ## State of contact with the resistive touch panel.
                            self.z = False
                            ## The current state of the FSM.
                            self.state = self.S1_CALIBRATION
                            ## PID object to calibrate M1
                            self.pid1= PID(4, 0.1, 0.1)
                            ## PID object to calibrate M2
                            self.pid2= PID(4, 0.1, 0.1)
                        
                        # State 1
                        elif(self.state == self.S1_CALIBRATION):
                            euler = self.IMU.euler()
                            self.E1.update()
                            self.E2.update()                    
                            if abs(euler[1]) <= 0.0625 and not self.E1_calibrated:
                                print('Encoder 1 calibrated.')
                                self.E1.set_position(0)
                                self.E1_calibrated = True
                            if abs(euler[2]) <= 0.0625 and not self.E2_calibrated:
                                print('Encoder 2 calibrated.')
                                self.E2.set_position(0)
                                self.E2_calibrated = True
                            if self.E1_calibrated and self.E2_calibrated:
                                print('Both encoders calibrated. Place the ball on the platform.')
                                self.state = S2_MEASURE
                            T_x = self.pid2.update(0, euler[1])
                            T_y = self.pid1.update(0, euler[2])
                            self.M1.set_duty(T_y)
                            self.M2.set_duty(T_x)
                                
                        # State 2   
                        elif(self.state == self.S2_MEASURE):               
                            self.z = self.RTP.scan_z()
                            # Update encoders
                            self.E1.update()
                            self.E2.update()
                            # Angular position of the platform [rad]
                            self.theta_x = (-1)*(self.E2.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            self.theta_y = (-1)*(self.E1.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            
                            # Angular velocity of the platform [rad/s]
                            self.theta_dot_x = (-1)*(self.E2.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            self.theta_dot_y = (-1)*(self.E1.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            
                            # Calculate the required duty levels for the motors
                            self.T_x = self.F1.update(0, self.theta_dot_x, 0, self.theta_x)
                            self.T_y = self.F2.update(0, self.theta_dot_y, 0, self.theta_y) 
            
                            # Set the duty levels of the motors
                            self.M1.set_duty(self.T_y)
                            self.M2.set_duty(self.T_x)
                            
                            # Contact detected - transition to State 3
                            if self.z:           
                                self.x, self.y, self.z = self.RTP.scan_rtp_fast()
                                # Convert RTP readings from mm to m
                                self.x = self.x/1000
                                self.y = self.y/1000
                                self.state = self.S3_CONTROL
                                continue
                            
                        # State 3: Control
                        elif(self.state == self.S3_CONTROL):
                            self.x_old = self.x
                            self.y_old = self.y
                            self.x, self.y, z = self.RTP.scan_rtp_fast()
                            # No contact detected - immediately transition to State 2
                            if not self.z:
                                self.state = 2
                                continue
                            # Update encoders
                            self.E1.update()
                            self.E2.update()
                            # Convert touchpad readings from mm to m
                            self.x = self.x/1000
                            self.y = self.y/1000
                            # Velocity of the ball [m/s]
                            self.x_dot = (self.x - self.x_old) * 1000 / self.interval
                            self.y_dot = (self.y - self.y_old) * 1000 / self.interval
                            # Angular position of the platform [rad]
                            self.theta_x = (-1)*(self.E2.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            self.theta_y = (-1)*(self.E1.get_position() * 2 * 3.14159 / 4000) * self.l_p / self.r_m
                            # Angular velocity of the platform [rad/s]
                            self.theta_dot_x = (-1)*(self.E2.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            self.theta_dot_y = (-1)*(self.E1.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.l_p / self.r_m
                            # Duty levels for the motors
                            self.M1.set_duty(self.F2.update(self.x, self.theta_y, self.x_dot, self.theta_dot_y))
                            self.M2.set_duty(self.F1.update(self.y, self.theta_x, self.y_dot, self.theta_dot_x))
                            fp.write('{:},{:},{:},{:}\n'.format(self.x_dot, self.theta_dot_x, self.x, self.theta_x))
                        
                        self.next_time = self.current_time + self.interval
         
        # Turn both motors off and exit
        except Exception as e:
              self.M1.set_duty(0)
              self.M2.set_duty(0)
              self.M1.disable()
              self.M2.disable()
              print('\n\nExiting Program\n\n')
              print(e)
