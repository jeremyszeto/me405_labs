'''
@file TouchScreenDriver.py
@brief A hardware driver for the resistive touch panel was created. Ultimately, the touch panel will be used to indicate
       the planar position of the ball on the platform for the final project. The driver makes it possible to independently 
       read the x, y, or z position. Here the z-component tells whether or not contact has been made with the screen.
       Settling time was taken into consideration when designing the methods and it is possible to read from a single 
       channel in less than 500 microseconds and all three channels in less than 1500 microseconds. The resulting
       x, y, and boolean value for z is returned as a tuple.
       The strict timing requirement is required for the incorporation of this code with the
       rest of the project.
@ details
@author Rebecca Rodriguez
@date March 7, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
from pyb import Pin, ADC
import utime

class ScreenDriver:
    '''
    @brief  This screen driver was developed for a resistive touch screen (800x480 pixels). The screen driver will return the position of the ball and whether or not the ball is in contact with the screen.
    @details The datasheet for the screen may be found at: http://www.buydisplay.com/download/manual/ER-TFT080-1_Datasheet.pdf/
    @author Rebecca Rodriguez
    @date March 7, 2021
    @copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
    '''
    
    def __init__(self, Ym_pin, Xm_pin, Yp_pin, Xp_pin, length, width, x_center, y_center):
        '''
        @brief    The constructor allows the user to select any four pins to communicate with
                  the resistive touch panel screen. 
        @details  
        @param     
        @param     
        @param              
        '''
        self.Ym_pin = Ym_pin 
        self.Xm_pin = Xm_pin 
        self.Yp_pin = Yp_pin 
        self.Xp_pin = Xp_pin 
        self.length = length
        self.width = width
        self.x_center = x_center
        self.y_center = y_center
        
    def xScan(self):
        '''
        @brief     This method determines the horizontal planar location of the contact point.
        @details   Pin x_p and x_m must be configured as a push-pull outputs. 
                   Pin x_p is set high and pin x_m is set low.
                   Pin y_p is set floating and the voltage is measured on y_m.
        @return X_pos Returns values within the range of the length specified in constructor. The reading is zero if the point of contact is in the middle of the touch panel.
        '''
        self.xm = Pin(self.Xm_pin, mode = Pin.OUT_PP) 
        self.xp = Pin(self.Xp_pin, mode = Pin.OUT_PP) 
        self.yp = Pin(self.Yp_pin, mode = Pin.IN) 
        self.ym = ADC(self.Ym_pin)
        
        self.xp.high()
        self.xm.low()
        
        utime.sleep_us(4)

        self.X_pos = self.ym.read()
        self.X_pos = (self.x_center - self.X_pos) * (self.length / (2000-190))

        return self.X_pos
        
    def yScan(self):
        '''
        @brief     This method determines whether contact is made with the screen or not.
        @details   Pin y_p and y_m must be configured as a push-pull outputs. 
                   Pin y_p is set high and pin y_m is set low.
                   Pin x_p is set floating and the voltage is measured on x_m.
        @return Y_pos Returns values within the range of the width specified in constructor. The reading is zero if the point of contact is in the middle of the touch panel.
        '''
        self.ym = Pin(self.Ym_pin, mode = Pin.OUT_PP) 
        self.yp = Pin(self.Yp_pin, mode = Pin.OUT_PP) 
        self.xp = Pin(self.Xp_pin, mode = Pin.IN) 
        self.xm = ADC(self.Xm_pin)
        
        self.yp.high()
        self.ym.low()
        
        utime.sleep_us(4)
        
        self.Y_pos = self.xm.read()
        self.Y_pos = (self.y_center - self.Y_pos) * (self.width / (1500-600))
        
        return self.Y_pos
        
    def zScan(self):
        '''
        @brief     This method determines the vertical location of the contact point.
        @details   Pin y_p and x_m must be configured as a push-pull outputs. 
                   Pin y_p is set high and pin x_m is set low.
                   The voltage is measured on pin y_m.
        @return self.contact  Return boolean value whether or not contact is made with the screen. If voltage is measured on pin y_m then there is contact with the screen.
        '''
        self.xm = Pin(self.Xm_pin, mode = Pin.OUT_PP) 
        self.yp = Pin(self.Xp_pin, mode = Pin.OUT_PP) 
        self.xp = Pin(self.Yp_pin, mode = Pin.IN) 
        self.ym = ADC(self.Ym_pin)
        
        self.yp.high()
        self.xm.low()
        
        utime.sleep_us(4)
        
        self.Z = self.ym.read()
        
        if self.Z > 100 and self.Z <4000:
            self.contact = True
        else:
            self.contact = False
            
        return self.contact
    
    def xyzScan(self):
        '''
        @brief     This method returns the coordinate measurements in the x- and y-axis
                   and boolean value for the z-axis.
        @return xyzScan The data collected is returned as a tuple.
        '''
        self.xScan()
        self.yScan()
        self.zScan()
        xyzScan = (self.X_pos,self.Y_pos,self.contact)
        
        return xyzScan 
    
    def quickScan(self):
        '''
        @brief  This method returns the coordinate measurements in the x- and y-axis
                   and boolean value for the z-axis in a tuple.
        @details The quick scan determines the planar position of an object on the touch screen in the quickest amount of time by using the least number of pin reconfigurations.
        @return xyzScan The data collected is returned as a tuple.
        '''
        self.xm = Pin(self.Xm_pin, mode = Pin.OUT_PP) 
        self.xp = Pin(self.Xp_pin, mode = Pin.OUT_PP) 
        self.yp = Pin(self.Yp_pin, mode = Pin.IN) 
        self.ym = ADC(self.Ym_pin)
        
        self.xp.high()
        self.xm.low()
        
        utime.sleep_us(4)

        self.X_pos = self.ym.read()
        self.X_pos = (self.x_center - self.X_pos) * (self.length / (2000-190))
        
        self.yp = Pin(self.Xp_pin, mode = Pin.OUT_PP) 
        self.xp = Pin(self.Yp_pin, mode = Pin.IN) 
        
        self.yp.high()
        self.xm.low()
        
        utime.sleep_us(4)
        
        self.Z = self.ym.read()
        
        if self.Z > 100 and self.Z <4000:
            self.contact = True
        else:
            self.contact = False
        
        self.ym = Pin(self.Ym_pin, mode = Pin.OUT_PP) 
        self.yp = Pin(self.Yp_pin, mode = Pin.OUT_PP) 
        self.xm = ADC(self.Xm_pin)
        
        self.yp.high()
        self.ym.low()
        
        utime.sleep_us(4)
        
        self.Y_pos = self.xm.read()
        self.Y_pos = (self.y_center - self.Y_pos) * (self.width / (1500-600))
        
        xyzScan = (self.X_pos,self.Y_pos,self.contact)
        
        return xyzScan 
    
        
if __name__ == '__main__':
    # Create pin objects used for interfacing with the screen driver
    pin_ym = Pin.board.PA0 
    pin_xm = Pin.board.PA1   
    pin_yp = Pin.board.PA6  
    pin_xp = Pin.board.PA7

    scc = ScreenDriver(pin_ym, pin_xm, pin_yp, pin_xp, 18.5,10.5,1414,1500)
    
    # Determine duration of polling individual channels
    startTime = utime.ticks_us()
    xScan = scc.xScan()
    finalTime = utime.ticks_us()
    print('Time [microseconds]: {:}, x-value [cm]: {:}'.format(utime.ticks_diff(finalTime,startTime),xScan))
    
    startTime = utime.ticks_us()
    yScan = scc.yScan()
    finalTime = utime.ticks_us()
    print('Time [microseconds]: {:}, y-value: [cm] {:}'.format(utime.ticks_diff(finalTime,startTime),yScan))
    
    startTime = utime.ticks_us()
    zScan = scc.zScan()
    finalTime = utime.ticks_us()
    print('Time [microseconds]: {:}, Point of contact? {:}'.format(utime.ticks_diff(finalTime,startTime),zScan))
    
    # Determine duration of polling all 3 channels
    startTime = utime.ticks_us()
    xyzScan = scc.xyzScan()
    finalTime = utime.ticks_us()
    print('Time [microseconds]: {:}, x: [cm] {:}, y: [cm] {:}, Point of contact? {:}'.format(utime.ticks_diff(finalTime,startTime),xyzScan[0],xyzScan[1],xyzScan[2]))
    
    startTime = utime.ticks_us()
    quickScan = scc.quickScan()
    finalTime = utime.ticks_us()
    print('Time [microseconds]: {:}, x: [cm] {:}, y: [cm] {:}, Point of contact? {:}'.format(utime.ticks_diff(finalTime,startTime),quickScan[0],quickScan[1],quickScan[2]))
    