'''
@file main_by_RR.py
@brief Closed-loop control feedback for balancing the ball on the platform.
@ details
@author Rebecca Rodriguez
@date June 11, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
from TouchScreenDriver import ScreenDriver
from motorDriver_working import MotorDriver
from bno055 import BNO055
from controller_by_RR import Controller
import machine
import pyb
import utime

print('Initializing resistive touch panel...')
## A touchPanelDriver object used control the touch panel.
scc = ScreenDriver(pyb.Pin.board.PA0, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA7, 18.5,10.5,1414,1500)

print('Initializing motors..')
## Motor nSLEEP pin used to enable the driver chip.
pin_nSLEEP = pyb.Pin.cpu.A15; # doesn't change
## Motor nFAULT pin used for fault detection.
#pin_nFAULT = pyb.Pin.cpu.B2;  # doesn't change

# Create a timer object used for PWM generation
tim = pyb.Timer(3, freq=20000) # doesn't change

# Create a motor object passing in the pins and timer for motor 1
# moe_1 = MotorDriver(pin_nSLEEP, pin_nFAULT, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, tim, 1, 2)
moe_1 = MotorDriver(pin_nSLEEP, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, tim, 1, 2)
# Enable motor driver for motor 1
moe_1.enable()
# Set duty cycle to zero for motor 1
moe_1.set_duty(0)
    
# Create a motor object passing in the pins and timer for motor 2
# moe_2 = MotorDriver(pin_nSLEEP, pin_nFAULT, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, tim, 3, 4)
moe_2 = MotorDriver(pin_nSLEEP, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, tim, 3, 4)
# Enable motor driver for motor 2
moe_2.enable()
# Set duty cycle to zero for motor 2
moe_2.set_duty(0)
 
print('Initializing IMU..')
# Specify I2C channel used
i2c = machine.I2C(1)
# Create an IMU object 
imu = BNO055(i2c)

print('Initializing the closed-loop controller..')
# Gain matrix in the form of [K_1 K_2 K_3 K_4]
Gains_M1 = [-3.28, 0,-9.09, 0]
#Gains = [-3.28, -0.27,-9.09, -4]
Gains_M2 = [-3.28, 0,-9.09, 0]
# Resistance of motor [ohms]
R = 2.21 
# Motor torque constant [mNm/A]
Kt = 13.8
#Voltage supplied to the motor [V]
Vdc = 12 
# Reduction gear ratio
N = 4
percent_duty_convFactor = (100 * R) / (N * Kt * Vdc)
gains1 = [-3.28, 0,-9.09, 0]
gains2 = [-3.28, 0,-9.09, 0]

## Resistive touch panel measured ball position (x) [m]
x, y = 0, 0

## Resistive touch panel measured ball velocity (x dot) [m/s]
x_dot, y_dot = 0, 0

## Platform angle (theta) [rad]
theta_x, theta_y = 0, 0

## Platform angular velocity (theta dot) [rad/s]
theta_dotx, theta_doty = 0, 0
# Conversion factor necessary for the calculation in the percent duty of the motor.
percent_duty_convFactor = (100 * R) / (N * Kt * Vdc)
# A Controller object used control each motor.
controller = Controller(Gains_M1, Gains_M2, percent_duty_convFactor, moe_1, moe_2, scc, imu)


if __name__ == '__main__': 
    while True:
        controller = Controller.run()
    