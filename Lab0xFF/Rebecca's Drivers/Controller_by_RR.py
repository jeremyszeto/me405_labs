'''
@file controller_by_RR.py
@brief Closed-loop control feedback for balancing the ball on the platform.
@ details
@author Rebecca Rodriguez
@date June 11, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
from TouchScreenDriver import ScreenDriver
from motorDriver_working import MotorDriver
import machine
from bno055 import BNO055
import pyb
import utime


class Controller:
    '''
    @brief  
    @details 
    @author Rebecca Rodriguez
    @date March 7, 2021
    @copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
    '''
    def __init__(self, gains1, gains2, percent_duty_convFactor, motor1, motor2, scc, imu):
        ## Gain matrix in the form of [K_1 K_2 K_3 K_4]
        self.gains1 = gains1
        self.gains2 = gains2
        
        ## Initial ball position (x, y) [m]
        self.x, self.y = 0, 0
        
        ## Initial ball velocity (x dot, y dot) [m/s]
        self.x_dot, self.y_dot = 0, 0
        
        ## Iniital platform angle [deg]
        self.theta_x, self.theta_y = 0, 0
        
        ## Platform angular velocity (theta dot x, theta dot y) [deg/s]
        self.theta_dotx, self.theta_doty = 0, 0
        
        self.motor1 = motor1
        self.motor2 = motor2
        self.scc = scc
        self.imu = imu
        
    
    def run(self):
        print('Please balance the ball at the center of the platform...')
        xyz = self.scc.quickScan()
        if xyz[2] == False:
            print('Please balance the ball at the center of the platform...')
        else:
            print('Thanks! Let the balancing begin!')
            self.correct_PWM()
            
        
    
    def correct_PWM(self):
        self.get_StateVar()
        
        self.Tm_M1 = -(self.Gains_M1[0]*self.yF + self.Gains_M1[1]*self.y_dot + 
                    self.Gains_M1[2]*self.theta_xF + self.Gains_M1[3]*self.theta_dotx)
        
        self.L_1 = self.percent_duty_convFactor * self.Tm_1

        self.motor1.set_duty(self.L)
        
        self.Tm_M2 = -(self.Gains_M2[0]*self.xF + self.Gains_M2[1]*self.x_dot + 
                    self.Gains_M2[2]*self.theta_yF + self.Gains_M2[3]*self.theta_doty)
        
        self.L_2 = self.percent_duty_convFactor * self.Tm_2

        self.motor2.set_duty(self.L)
        
        
        
    
    def get_StateVar(self):
        ''' Obtain position and velocity of the ball, as well as the angle and angular velocity of the platform.
        '''
        self.interval = 100
        t_initial = utime.ticks_ms()
        t_final = utime.ticks_add(t_initial, self.interval)
        self.xyz = self.scc.quickScan()
        self.euler = self.imu.euler()
        self.x = self.xyz[0]
        self.y = self.xyz[1]
        self.theta_x = self.euler[1]
        self.theta_y = self.euler[2]
        
        Finished = False
        
        while Finished == False:
            
            if utime.ticks_ms() >= t_final:
                self.xyz = self.scc.quickScan()
                self.euler = self.imu.euler()
                self.xF = self.xyz[0]
                self.yF = self.xyz[1]
                self.theta_xF = self.euler[1]
                self.theta_yF = self.euler[2]
                
                # Calculates velocities of the ball in the x and y direction and angular velocity of the platform
                self.x_dot = (self.xF - self.x)/ (self.interval * 10)
                self.y_dot = (self.yF - self.y)/ (self.interval * 10)
                self.theta_dotx = (self.theta_xF - self.theta_x)/ (self.interval * 10)
                self.theta_doty = (self.theta_yF - self.theta_y)/ (self.interval * 10)
                
                Finished = True
                
            else:
                pass
            
        return self.xF, self.yF, self.theta_xF, self.theta_yF, self.x_dot, self.y_dot, self.theta_dotx, self.theta_doty