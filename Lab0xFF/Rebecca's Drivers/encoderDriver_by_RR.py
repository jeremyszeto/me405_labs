'''
@file encoderDriver.py
@brief This encoder driver contains multiple methods to update and set the position of the encoder. 
       In addition, the encoder driver is designed to obtain data from the two DC motors of the balancing platform independently.
       This means that two objects may be created from this class that can independently obtain data from the two separate motors.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date March 7, 2021
'''
import pyb
import utime

class EncoderDriver:
    '''
    @brief      
    @details    
    '''
    
    def __init__(self, CH1_pin, CH2_pin, timer):
        '''
        @brief    The constructor creates the channel info, sets the timers, and zeros the variables. 
        @details  The period and prescaler is specified by the user. An appropriate prescaler must be used
                  so that the timer will count once for every tick. In addition, the period value
                  must be selected to hold the largest possible 16-bit number.
        @param     CH1_pin
        @param     CH2_pin
        @param     timer          
        '''
        # Setup timer for encoder 
        self.timer =  pyb.Timer(timer)
        
        # Set up period
        self.period = 65535
        
        # Period is max possible 16-bit number
        self.time = self.timer.init(prescaler = 0, period = self.period)
        
        # Set up pins
        self.pin1 = pyb.Pin(CH1_pin)
        self.pin2 = pyb.Pin(CH2_pin)
         
        # Setup timer channel for polling both channels
        self.CH1 = self.timer.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.CH2 = self.timer.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
           
        # Use a timer to "poll" both encoder channels
        # Check encoder value every rising edge of clock
        # Timer will automatically count up/down when a state change occurs
        # Counter needs to be fast enough to catch software transitions
        self.counter = self.timer.counter()
        
        # Setup initial position
        self.position = 0
        
        # Setup delta position
        self.delta_position = 0
        print('Creating an encoder driver')
        
    def update(self):
        '''
        @brief   Runs one iteration of the task. Returns the encoder's updated position.   
        @details    
        '''
        # Read timer count to get number of encoder ticks (need 2 valid readings from each state)
        self.previous_counter = self.counter
        self.counter = self.timer.counter()
        
        # Calculate delta assuming update has already occurred
        # Subtract old count to get delta
        self.delta = self.counter - self.previous_counter
        self.mag_delta = abs(self.delta)
        
        # Fix bad deltas if they exceed a certain amount
        if(self.mag_delta > self.period/2):
        # "Fix" delta- bad deltas overflow
            if(self.delta > 0):
                # Subtract delta by 8
                self.delta_position = self.delta - self.period               
            elif(self.delta < 0):
                # Add delta by 8 
                self.delta_position = self.delta + self.period              

        elif(self.mag_delta < self.period/2):
            # Good delta
            self.delta_position = self.delta
            
        else:
            self.delta_position = 0

        # Add change in position (delta_position) to original position
        self.position += self.delta_position
        
    def get_position(self):
        '''
        @brief    Returns most recently updated position of the encoder  
        @details    
        '''
        return self.position      
        
    def set_position(self):
        '''
        @brief     Sets the position of the encoder to zero.
        @details  
        '''
        self.position = 0
        
    def get_delta(self):
        '''
        @brief     Returns delta value. 
        @details   Returns the difference in position between the two most recent calls to update.
        '''
        return self.delta

## An encoder task object
#
#  Details
#  @author Rebecca Rodriguez
#  @date October 26, 2020   
class EncoderTask(EncoderDriver):
    '''
    @brief      A finite state machine to control the EncoderTask.
    @details    This class implements a finite state machine to control the
                EncoderTask.
               '''             
	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_UPDATE  = 1    
    
    def __init__(self, interval, enc):
        '''
        @brief           Creates an encoder task object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        pass
    
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Run ' +str(self.runs) + ': State 0 - Initializing')
                self.transitionTo(self.S1_GET_UPDATE)
                
             elif(self.state == self.S1_GET_UPDATE):
                # Run State 1 Code
                self.EncoderDriver.update()
                # self.update()
                print('Run ' +str(self.runs) + ': State 1 - Update Encoder Position')
                self.transitionTo(self.S1_GET_UPDATE)
            
             self.runs += 1
            
             # Specifying the next time the task will run
             self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
               

if __name__ == '__main__':
    # create pin objects used for interfacing with the encoder driver
    pin_IN1 = pyb.Pin.cpu.B6;    # use timer 4 channel 1
    pin_IN2 = pyb.Pin.cpu.B7;    # use timer 4 channel 2
    pin_IN3 = pyb.Pin.cpu.C6;    # use timer 8 channel 3
    pin_IN4 = pyb.Pin.cpu.C7;    # use timer 8 channel 4
    
    # create a timer object
    tim_1_2 = 4;                      # use with pin IN1 or IN2
    tim_3_4 = 8;                      # use with pin IN3 or IN4
    
    # create an encoder object passing in the pins and timer for motor 1
    enc1 = EncoderDriver(pin_IN1, pin_IN2, tim_1_2)
    
    # create an encoder object passing in the pins and timer for motor 2
    enc2 = EncoderDriver(pin_IN3, pin_IN4, tim_3_4)
    
    task1 = EncoderTask(0.01, enc2)
    