'''
@file motorDriver.py
@brief This motor driver is designed to control the two DC motors of the balancing platform with pulse width modulation.
       Two objects may be created from this class that can independently control the two separate motors.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date March 7, 2021
'''
import pyb
import micropython
import utime

## Allcates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

class MotorDriver:
    '''
    @brief         
    @details    
    '''
    
    def __init__(self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN2_pin, timer, CH1, CH2):
        '''
        @brief     The motor driver is initialized by setting both pins as PWM channels. This makes it possible to adjust the duty cycle of each motor.
        @param   nSLEEP_pin  The pyb.Pin object for the nSLEEP pin of the motor driver (DRV8847).
        @param   nFAULT_pin  The pyb.Pin object for the nFAUKT pin of the motor driver (DRV8847).
        @param   IN1_pin     The pyb.Pin object for the channel 1 pin of the motor driver (DRV8847).
        @param   IN2_pin     The pyb.Pin object for the channel 2 of the motor driver (DRV8847).
        @param   timer       The timer object used for PWM generation that drives both channels to control a single motor.
        @param   CH1         The associated timer channel object used for PWM generation. 
        @param   CH2         The second associated timer channel object used for PWM generation. 
        '''

        self.nSLEEP_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.tim3 = timer
        self.nFAULT_pin = pyb.Pin(nFAULT_pin)
        
        self.IN1_pin = pyb.Pin(IN1_pin)
        self.IN2_pin = pyb.Pin(IN2_pin)
        
        self.CH1 = CH1
        self.CH2 = CH2
        
        self.faultDetected = False
    
        if IN1_pin == pyb.Pin.cpu.B4 and IN2_pin == pyb.Pin.cpu.B5:
            print('Creating a motor driver for motor 1')
        elif IN1_pin == pyb.Pin.cpu.B0 and IN2_pin == pyb.Pin.cpu.B1:
            print('Creating a motor driver for motor 2')
        else:
            print('Creating a motor driver failed.')
            
        # Create external interrupt for fault pin
        self.extint = pyb.ExtInt(self.nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.faultDet)
        # Setup channels 1 and 2 for PWM to control the motors
        self.t_ch1 = self.tim3.channel(self.CH1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.t_ch2 = self.tim3.channel(self.CH2, pyb.Timer.PWM, pin = self.IN2_pin)
            
    def enable(self):
        '''
        @brief     Sets nSLEEP pin to high to enable the motor.
        '''
        self.extint.disable()
        self.nSLEEP_pin.high()
        utime.sleep_us(50)
        self.extint.enable()
        print('Enabling motor')
        
    def disable(self):
        '''
        @brief     The nSLEEP_pin is pulled low so that the DC motor enters sleep mode. 
        '''
        self.nSLEEP_pin.low()
        print('Disabling motor')
        
    
    def faultDet(self):
        '''
        @brief  Sets the 'fault detected' flag to True and disables the motor for safety.    
        '''
        self.disable()
        self.setDuty(0)
        self.pinFault.high()
        self.faultDetected = True
        print('Fault detected.')
        
    def clearFault(self):
        '''
        @brief Enables the motor and clears the fault.
        '''
        self.enable()
        self.faultDetected = False
        print('Fault cleared.')
        
    def set_duty(self, duty):
        '''
        @brief   Set duty cycle of motor to control speed since motors controlled by PWM.   
        @param   duty The duty limits of the motor are set to -100 to 100.
        '''
        if duty > 0 or duty == 0:
            # Motor forward
            self.t_ch2.pulse_width_percent(0)
            if duty > 100:
                self.t_ch1.pulse_width_percent(100)
            else:
                self.t_ch1.pulse_width_percent(duty)
            
        elif duty < 0:
            # Motor in reverse
            self.t_ch1.pulse_width_percent(0)
            if duty < -100:
                self.t_ch2.pulse_width_percent(100)
            else:
                self.t_ch2.pulse_width_percent(-1 * duty)
          
        
        
if __name__ == '__main__':
    # create pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin.cpu.A15; # doesn't change
    pin_nFAULT = pyb.Pin.cpu.B2;  # doesn't change
    pin_IN1 = pyb.Pin.cpu.B4;     # use timer 3 channel 1
    pin_IN2 = pyb.Pin.cpu.B5;     # use timer 3 channel 2
    pin_IN3 = pyb.Pin.cpu.B0;     # use timer 3 channel 3
    pin_IN4 = pyb.Pin.cpu.B1;     # use timer 3 channel 4
    
    
    # create a timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000) # doesn't change
    
    # create a timer channel object used for PWM generation
    ch1 = 1
    ch2 = 2
    ch3 = 3
    ch4 = 4
    
    # create a motor object passing in the pins and timer for motor 1
    moe_1 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, tim, ch1, ch2)
    #moe_1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, ch1, ch2)
    
    # enable motor driver for motor 1
    moe_1.enable()
    
    # set duty cycle for motor 1
    moe_1.set_duty(0)
        
    # create a motor object passing in the pins and timer for motor 2
    moe_2 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, pin_IN4, tim, ch3, ch4)
    # moe_2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, ch3, ch4)
    
    # enable motor driver for motor 2
    moe_2.enable()
    
    # set duty cycle for motor 2
    moe_2.set_duty(0)   
    